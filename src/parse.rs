use std::{
    cell::RefCell,
    convert::TryFrom,
    io::BufWriter,
    ops::{Deref, DerefMut},
    rc::Rc,
    str::FromStr,
};
use std::{fmt, io};

use html5ever::{
    local_name, parse_document, parse_fragment,
    rcdom::{Node, NodeData, RcDom},
    serialize::{Serialize, TraversalScope},
    tendril::TendrilSink,
    tree_builder::TreeBuilderOpts,
    Attribute, LocalName, ParseOpts, QualName,
};
use log::{debug, error, trace, warn};
use url::Url;

use crate::types::{
    Class, Document, Fragment, Image, Item, ParentRelationship, PropertyValue, Relation,
    RelationshipKind, ValueKind,
};
use regex::Regex;

lazy_static::lazy_static! {
    static ref RE_CLASS_NAME: Regex = Regex::new(r#"^(?P<prefix>(h|p|u|dt|e))-(?P<name>([a-z0-9]+-)?[a-z]+(-[a-z]+)*)$"#).unwrap();
}

/// Represents a Microformats2 parser.
pub struct Parser {
    dom: RcDom,
    document: Rc<RefCell<Document>>,
    url: Url,
    base_url: Url,
}

/// A Facade over `Node` to invoke some common operations.
struct Element {
    pub attributes: RefCell<Vec<Attribute>>,
    pub name: String,
    pub node: Rc<Node>,
}

impl fmt::Debug for Element {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let html = extract_html(
            Rc::clone(&self.node),
            "http://indieweb.org".parse().unwrap(),
        )
        .map_err(|_e| std::fmt::Error {})?;
        f.debug_struct("Element")
            .field("attributes", &self.attributes)
            .field("name", &self.name)
            .field("node", &html)
            .finish()
    }
}

impl TryFrom<Rc<Node>> for Element {
    type Error = crate::Error;

    fn try_from(value: Rc<Node>) -> Result<Self, Self::Error> {
        if let NodeData::Element {
            ref attrs,
            ref name,
            ..
        } = value.data
        {
            Ok(Self {
                attributes: RefCell::clone(attrs),
                name: name.local.to_string(),
                node: Rc::clone(&value),
            })
        } else {
            Err(crate::Error::NotAnElement)
        }
    }
}

impl Element {
    /// Resolves an attribute's value.
    pub fn attribute(&self, attr_name: &str) -> Option<String> {
        self.maybe_attribute(attr_name).filter(|v| !v.is_empty())
    }

    pub fn maybe_attribute(&self, attr_name: &str) -> Option<String> {
        attribute_value(RefCell::clone(&self.attributes), attr_name)
    }

    pub fn has_attribute(&self, attr_name: &str) -> bool {
        self.maybe_attribute(attr_name).is_some()
    }

    pub fn classes(&self) -> Vec<String> {
        self.attribute("class")
            .map(|class_str| {
                class_str
                    .split_ascii_whitespace()
                    .map(|s| s.to_owned())
                    .collect::<Vec<_>>()
            })
            .unwrap_or_default()
    }

    pub fn has_class(&self, class_name: &str) -> bool {
        self.classes().contains(&class_name.to_owned())
    }

    pub fn property_classes(&self) -> Option<Vec<PropertyClass>> {
        self.attribute("class")
            .map(PropertyClass::list_from_string)
            .filter(|list| !list.is_empty())
    }

    pub fn is_property_element(&self) -> bool {
        self.property_classes().is_some()
    }

    /// Determines if this element is showing any h- (Microformat item) properties.
    pub fn is_microformat_item(&self) -> bool {
        self.property_classes()
            .unwrap_or_default()
            .iter()
            .any(|p| p.is_root())
    }

    pub fn microformat_item_types(&self) -> Vec<Class> {
        PropertyClass::extract_root_classes(self.property_classes().unwrap_or_default())
    }
}

fn ignored_elements() -> Vec<String> {
    vec![
        "script".to_owned(),
        "style".to_owned(),
        "template".to_owned(),
    ]
}

#[derive(thiserror::Error, Debug)]
pub enum RelationFromElementError {
    #[error("The provided element {0:?} is not a valid for relationship mapping.")]
    NotAValidElement(String),

    #[error("The provided element is missing a URL to reference.")]
    MissingURL,

    #[error("The provided element does not have an attribute with a 'rel' value.")]
    NotLinkRelation,
}

struct UrlRelation((Url, Relation));

impl UrlRelation {
    fn try_from_element(value: Rc<Element>, base_url: &Url) -> Result<Self, crate::Error> {
        if !["a", "link"].contains(&value.name.as_str()) {
            return Err(crate::Error::Relation(
                RelationFromElementError::NotAValidElement(value.name.clone()),
            ));
        }

        if !value.has_attribute("rel") {
            return Err(crate::Error::Relation(
                RelationFromElementError::NotLinkRelation,
            ));
        }

        let url_str = if let Some(v) = value.maybe_attribute("href") {
            v
        } else {
            return Err(crate::Error::Relation(RelationFromElementError::MissingURL));
        };

        url_str
            .parse()
            .or_else(|_| {
                if url_str.is_empty() {
                    Ok(base_url.clone())
                } else {
                    base_url.join(&url_str)
                }
            })
            .map_err(crate::Error::Url)
            .and_then(|url| {
                Ok(Self((
                    url,
                    Relation {
                        rels: value
                            .attribute("rel")
                            .map(|v| {
                                v.split_ascii_whitespace()
                                    .map(|s| s.to_string())
                                    .collect::<Vec<_>>()
                            })
                            .map(|mut l| {
                                l.dedup();
                                l
                            })
                            .unwrap_or_default(),
                        hreflang: value.attribute("hreflang").map(|s| s.trim().to_string()),
                        media: value.attribute("media").map(|s| s.trim().to_string()),
                        title: value.attribute("title").map(|s| s.trim().to_string()),
                        r#type: value.attribute("type").map(|s| s.trim().to_string()),
                        text: Some(extract_text(Rc::clone(&value.node), base_url.clone())?)
                            .filter(|v| !v.is_empty()),
                    },
                )))
            })
    }
}

impl Parser {
    /// Merges a path component into a URL (or resolves it as a URL if it's independent)
    pub fn resolve_url(&self, url_str: &str) -> Result<Url, crate::Error> {
        trace!("Merging {:?} to {:?}", url_str, self.url.to_string());
        if url_str.is_empty() {
            Ok(self.base_url.clone())
        } else {
            self.base_url
                .join(url_str)
                .or_else(|_| url_str.parse())
                .map_err(crate::Error::Url)
        }
    }

    /// Resolves a DOM parser using the expected options.
    pub fn dom_from_html(html: &str) -> Result<RcDom, crate::Error> {
        parse_document(
            RcDom::default(),
            ParseOpts {
                tree_builder: TreeBuilderOpts {
                    drop_doctype: true,
                    ..Default::default()
                },
                ..Default::default()
            },
        )
        .from_utf8()
        .read_from(&mut html.as_bytes())
        .map_err(crate::Error::IO)
    }

    /// Resolves an HTML fragment as a DOM using expected options.
    pub fn fragment_from_html(html: &str) -> Result<RcDom, crate::Error> {
        parse_fragment(
            RcDom::default(),
            ParseOpts {
                tree_builder: TreeBuilderOpts {
                    drop_doctype: true,
                    ..Default::default()
                },
                ..Default::default()
            },
            QualName {
                prefix: None,
                ns: "".into(),
                local: "div".into(),
            },
            vec![],
        )
        .from_utf8()
        .read_from(&mut html.as_bytes())
        .map_err(crate::Error::IO)
    }

    /// Creates a new parser with the provided document URL and HTML
    pub fn new(html: &str, url: Url) -> Result<Self, crate::Error> {
        Self::dom_from_html(html).map(|dom| Self {
            base_url: url.clone(),
            url,
            dom,
            document: Rc::default(),
        })
    }

    /// Begins the operation of parsing the document tree.
    pub fn parse(&mut self) -> Result<Document, crate::Error> {
        self.walk_over_children(
            Rc::clone(&self.dom.document),
            Rc::new(ParentRelationship::Root),
        )
        .map(|_| Rc::deref(&self.document).borrow().to_owned())
    }

    fn parse_rel(&mut self, rel_element: Rc<Element>) {
        if let Ok(UrlRelation((url, rel))) =
            UrlRelation::try_from_element(Rc::clone(&rel_element), &self.base_url)
        {
            self.document
                .borrow_mut()
                .deref_mut()
                .add_relation(url, rel);
        }
    }

    fn walk_over_children(
        &mut self,
        parent_node: Rc<Node>,
        current_item: Rc<ParentRelationship>,
    ) -> Result<(), crate::Error> {
        trace!("Walking over the children of the current node.");
        let child_nodes = parent_node.children.borrow();
        child_nodes
            .deref()
            .iter()
            .filter_map(|c| Element::try_from(Rc::clone(c)).ok().map(Rc::new))
            .try_for_each(|element| {
                if element.name == "base" && self.base_url == self.url {
                    if let Some(url) = element.attribute("href").and_then(|u| u.parse().ok()) {
                        trace!("Updated the base URL of this document to be {:?}", url);
                        self.base_url = url;
                        Ok(())
                    } else {
                        Ok(())
                    }
                } else if !ignored_elements().contains(&element.name) {
                    if ["a", "link"].contains(&element.name.as_str())
                        && element.has_attribute("rel")
                    {
                        self.parse_rel(Rc::clone(&element))
                    }
                    self.parse_element_for_mf2(Rc::clone(&element), Rc::clone(&current_item))
                } else {
                    Ok(())
                }
            })
    }

    fn parse_element_for_mf2(
        &mut self,
        element: Rc<Element>,
        parent_item: Rc<ParentRelationship>,
    ) -> Result<(), crate::Error> {
        if let Some(property_classes) = element.property_classes() {
            trace!(
                "This element has {:?} as property classes.",
                property_classes
            );

            let non_root_property_classes = property_classes
                .iter()
                .filter(|p| !p.is_root())
                .cloned()
                .collect::<Vec<_>>();

            if element.is_microformat_item() {
                // We're processing a new item.
                let types = element.microformat_item_types();
                trace!(
                    "New root item encountered; has the root classes {:?}",
                    types
                );

                let current_item = ParentRelationship::create_child_item(
                    Rc::clone(&parent_item),
                    Rc::clone(&self.document),
                    &types,
                );

                if let Some(id_value) = element.attribute("id") {
                    trace!("Set the ID of this element to be {:?}", id_value);
                    current_item.borrow_mut().id = Some(id_value);
                } else {
                    trace!("No value for the ID was defined in the DOM.");
                }

                self.walk_over_children(
                    Rc::clone(&element.node),
                    Rc::new(ParentRelationship::Child(Rc::clone(&current_item))),
                )?;

                self.resolve_implied_properties(Rc::clone(&element), Rc::clone(&current_item));

                if let Some(parent_item) = parent_item.resolve_item() {
                    self.associate_item_to_parent_as_property(
                        Rc::clone(&parent_item),
                        Rc::clone(&current_item),
                        non_root_property_classes,
                    );
                }

                self.resolve_value_for_property_item(
                    Rc::clone(&current_item),
                    Rc::clone(&element.node),
                );

                trace!(
                    "Completed base processing of the new item {:#?}",
                    current_item
                );
            } else {
                if let Some(item) = parent_item.resolve_item() {
                    trace!(
                        "No root item classes detected; working as a property to the current item {:#?}.", item
                    );
                    self.resolve_explicit_properties(
                        Rc::clone(&element),
                        Rc::clone(&item),
                        non_root_property_classes,
                    )?;
                    self.resolve_implied_properties(Rc::clone(&element), Rc::clone(&item));
                    self.resolve_value_for_property_item(
                        Rc::clone(&item),
                        Rc::clone(&element.node),
                    );
                } else {
                    error!("No parent item DETECTED");
                }
                self.walk_over_children(Rc::clone(&element.node), Rc::clone(&parent_item))?;
            }

            Ok(())
        } else {
            trace!("No property classes detected on this element.");
            self.walk_over_children(Rc::clone(&element.node), parent_item)
        }
    }

    fn resolve_implied_properties(&self, element: Rc<Element>, item: Rc<RefCell<Item>>) {
        if item.deref().borrow().has_nested_microformats() {
            return;
        };

        if let Ok(Some(implied_name_value)) =
            self.resolve_implied_name(Rc::clone(&element), Rc::clone(&item))
        {
            let name = PropertyValue::Plain(implied_name_value);
            item.borrow_mut().append_property("name", name);
        }

        if let Some(implied_url_value) = resolve_implied_url(Rc::clone(&element), Rc::clone(&item))
            .and_then(|u| self.resolve_url(&u).ok())
            .map(|url| PropertyValue::Url(url))
        {
            item.borrow_mut().append_property("url", implied_url_value);
        }

        if let Some(implied_photo_value) =
            self.resolve_implied_photo(Rc::clone(&element), Rc::clone(&item))
        {
            item.borrow_mut()
                .append_property("photo", implied_photo_value);
        }
    }

    fn resolve_value_for_property_item(&self, item: Rc<RefCell<Item>>, node: Rc<Node>) {
        trace!("Attempting to resolve the value of the item {:#?}", item);
        let parent = item.deref().borrow().parent();

        let value = match parent.deref() {
            ParentRelationship::Property(relationship_kind, _) => {
                let props = item.deref().borrow().properties.borrow().deref().clone();
                trace!("Determined this item to be of a property with the relationship {:#?} with the properties {:#?}", relationship_kind, props);
                match relationship_kind {
                    RelationshipKind::Plain(property_name) => props
                        .get("name")
                        .cloned()
                        .or_else(|| props.get(property_name).cloned())
                        .or_else(|| {
                            extract_only_text(node, self.base_url.clone())
                                .map(|t| t.trim().to_string())
                                .map(PropertyValue::Plain)
                                .ok()
                                .map(|v| vec![v])
                        })
                        .and_then(|v| {
                            v.iter().cloned().find_map(|v| {
                                if let PropertyValue::Plain(pv) = v {
                                    Some(pv)
                                } else {
                                    None
                                }
                            })
                        })
                        .map(|sv| ValueKind::Plain(sv.to_owned())),
                    RelationshipKind::URL(property_name) => props
                        .get("url")
                        .or_else(|| props.get(property_name))
                        .and_then(|v| {
                            v.iter().find_map(|v| {
                                if let PropertyValue::Url(u) = v {
                                    Some(u.to_owned())
                                } else {
                                    None
                                }
                            })
                        })
                        .map(|sv| ValueKind::Url(sv)),
                    RelationshipKind::Datetime(property_name) => props
                        .get(property_name)
                        .and_then(|v| {
                            v.iter().find_map(|v| {
                                if let PropertyValue::Temporal(dt) = v {
                                    Some(dt)
                                } else {
                                    None
                                }
                            })
                        })
                        .map(|sv| sv.to_string())
                        .map(|sv| ValueKind::Plain(sv)),
                    RelationshipKind::HTML(property_name) => props
                        .get(property_name)
                        .and_then(|v| {
                            v.iter().find_map(|v| {
                                if let PropertyValue::Fragment(Fragment { ref value, .. }) = v {
                                    Some(value.to_owned())
                                } else {
                                    None
                                }
                            })
                        })
                        .map(|sv| ValueKind::Plain(sv.to_owned())),
                }
            }
            // FIXME: Should this just resolve the URL of the associated item?
            ParentRelationship::Child(_) | ParentRelationship::Root => None,
        };

        trace!("Setting the value of the item to be {:#?}", value);
        item.borrow_mut().deref_mut().value = value;
    }

    // This will make it go from being a child to a property
    // This will also set `value` based on the kind of property it is.
    // FIXME: Remove logic for setting `value` into separate method.
    fn associate_item_to_parent_as_property(
        &self,
        parent_item: Rc<RefCell<Item>>,
        current_item: Rc<RefCell<Item>>,
        property_classes: Vec<PropertyClass>,
    ) {
        if property_classes.is_empty() {
            trace!("This is not a property element due to the lack of provided classes; skipping.");
            return;
        } else {
            debug!(
                "Associating {:#?} with the classes {:#?} to {:#?}",
                current_item, property_classes, parent_item
            );

            parent_item
                .borrow_mut()
                .deref_mut()
                .remove_child(Rc::clone(&current_item));

            for property in property_classes {
                trace!("Processing the associative property {:?}", property);

                let (property_name, property_relationship): (String, ParentRelationship) =
                    if let PropertyClass::Plain(property_name) = property {
                        (
                            property_name.to_owned(),
                            ParentRelationship::Property(
                                RelationshipKind::Plain(property_name),
                                Rc::clone(&parent_item),
                            ),
                        )
                    } else if let PropertyClass::Linked(property_name) = property {
                        (
                            property_name.to_owned(),
                            ParentRelationship::Property(
                                RelationshipKind::URL(property_name),
                                Rc::clone(&parent_item),
                            ),
                        )
                    } else if let PropertyClass::Timestamp(property_name) = property {
                        (
                            property_name.to_owned(),
                            ParentRelationship::Property(
                                RelationshipKind::Datetime(property_name),
                                Rc::clone(&parent_item),
                            ),
                        )
                    } else if let PropertyClass::Hypertext(property_name) = property {
                        (
                            property_name.to_owned(),
                            ParentRelationship::Property(
                                RelationshipKind::HTML(property_name),
                                Rc::clone(&parent_item),
                            ),
                        )
                    } else {
                        panic!("invalid")
                    };
                current_item
                    .borrow_mut()
                    .deref_mut()
                    .set_parent(Rc::new(property_relationship));

                parent_item.borrow_mut().deref_mut().append_property(
                    &property_name,
                    PropertyValue::Item(Rc::clone(&current_item)),
                );
            }
        }
    }

    // Resolves the properties directly associated to this item (that haven't been associated as its parent).
    fn resolve_explicit_properties(
        &mut self,
        element: Rc<Element>,
        item: Rc<RefCell<Item>>,
        classes: Vec<PropertyClass>,
    ) -> Result<(), crate::Error> {
        if classes.is_empty() {
            return Ok(());
        }

        let groomed_classes = classes
            .iter()
            .filter_map(|class| match &class {
                PropertyClass::Root(_) => None,
                PropertyClass::Plain(p)
                | PropertyClass::Linked(p)
                | PropertyClass::Timestamp(p)
                | PropertyClass::Hypertext(p) => {
                    if let Some(true) = item
                        .deref()
                        .borrow()
                        .properties
                        .deref()
                        .borrow()
                        .get(p)
                        .map(|values| values.contains(&PropertyValue::Item(Rc::clone(&item))))
                    {
                        None
                    } else {
                        Some(class)
                    }
                }
            })
            .collect::<Vec<_>>();
        trace!(
            "Resolving properties to add from {:#?} (only {:#?}) to {:#?}",
            classes,
            groomed_classes,
            item
        );

        let additional_property_values = groomed_classes
            .into_iter()
            .try_fold::<_, _, Result<_, crate::Error>>(Vec::default(), |mut acc, class| {
                trace!("Parsing {:?} as the property.", class);
                let parsed_value = if let PropertyClass::Plain(ref property_name) = class {
                    Some((
                        property_name.to_owned(),
                        self.parse_plain_property(Rc::clone(&element))?,
                    ))
                } else if let PropertyClass::Linked(ref property_name) = class {
                    Some((
                        property_name.to_owned(),
                        self.parse_linked_property(Rc::clone(&element))?,
                    ))
                } else if let PropertyClass::Timestamp(ref property_name) = class {
                    Some((
                        property_name.to_owned(),
                        self.parse_temporal_property(Rc::clone(&element)),
                    ))
                } else if let PropertyClass::Hypertext(ref property_name) = class {
                    Some((
                        property_name.to_owned(),
                        self.parse_html_property(Rc::clone(&element))?,
                    ))
                } else {
                    None
                };

                if let Some((name, values)) = parsed_value.filter(|(_, values)| !values.is_empty())
                {
                    acc.extend_from_slice(
                        &values
                            .into_iter()
                            .map(|value| (name.clone(), value))
                            .collect::<Vec<_>>(),
                    );
                    Ok(acc)
                } else {
                    Ok(acc)
                }
            })?;

        for (property_name, property_value) in additional_property_values {
            trace!(
                "Adding the value {:?} to the property {:?} in the item {:#?}",
                property_value,
                property_name,
                item
            );

            item.borrow_mut()
                .append_property(&property_name, property_value.clone());

            if let PropertyValue::Fragment { .. } = property_value {
                self.walk_over_children(
                    Rc::clone(&element.node),
                    Rc::new(ParentRelationship::Child(Rc::clone(&item))),
                )?
            }
        }

        item.borrow_mut().deref_mut().concatenate_times();
        Ok(())
    }

    fn parse_plain_property(
        &self,
        element: Rc<Element>,
    ) -> Result<Vec<PropertyValue>, crate::Error> {
        let value = if let Some(body) = value_class::parse(
            Rc::clone(&element.node),
            self.base_url.clone(),
            value_class::TypeHint::Plain,
        ) {
            return Ok(vec![body]);
        } else if let Some(v) = resolve_attribute_value_from_expected_property_element(
            Rc::clone(&element),
            &["abbr", "link"],
            "title",
        ) {
            vec![PropertyValue::Plain(v)]
        } else if let Some(v) = resolve_attribute_value_from_expected_property_element(
            Rc::clone(&element),
            &["data", "input"],
            "value",
        ) {
            vec![PropertyValue::Plain(v)]
        } else if let Some(v) = resolve_attribute_value_from_expected_property_element(
            Rc::clone(&element),
            &["img", "area"],
            "alt",
        ) {
            vec![PropertyValue::Plain(v)]
        } else {
            vec![PropertyValue::Plain(
                extract_only_text(Rc::clone(&element.node), self.base_url.clone())
                    .map(|v| v.trim().to_string())?,
            )]
        };

        Ok(value)
    }

    fn parse_html_property(
        &self,
        element: Rc<Element>,
    ) -> Result<Vec<PropertyValue>, crate::Error> {
        let html = extract_html_from_children(Rc::clone(&element.node), self.base_url.clone())?
            .trim()
            .to_string();
        let value = extract_text(Rc::clone(&element.node), self.base_url.clone())?
            .trim()
            .to_string();
        // FIXME: Resolve this from either <html lang=> or elsewhere.
        let lang = self
            .document
            .deref()
            .borrow()
            .lang
            .clone()
            .or_else(|| element.attribute("lang"));

        Ok(vec![PropertyValue::Fragment(Fragment {
            html,
            value,
            lang,
        })])
    }

    fn parse_linked_property(
        &self,
        element: Rc<Element>,
    ) -> Result<Vec<PropertyValue>, crate::Error> {
        let value = if let Some(v) = resolve_maybe_attribute_value_from_expected_element(
            Rc::clone(&element),
            &["a", "area", "link"],
            "href",
        )
        .or_else(|| {
            resolve_maybe_attribute_value_from_expected_element(
                Rc::clone(&element),
                &["audio", "video", "source", "iframe"],
                "src",
            )
        })
        .or_else(|| {
            resolve_maybe_attribute_value_from_expected_element(
                Rc::clone(&element),
                &["video"],
                "poster",
            )
        })
        .or_else(|| {
            resolve_maybe_attribute_value_from_expected_element(
                Rc::clone(&element),
                &["object"],
                "data",
            )
        }) {
            Some(v)
        } else if let Some(value) = value_class::parse(
            Rc::clone(&element.node),
            self.base_url.clone(),
            value_class::TypeHint::Plain,
        )
        .filter(|s| !s.is_empty())
        .and_then(|value| {
            if let PropertyValue::Plain(s) = value {
                Some(s)
            } else {
                None
            }
        }) {
            Some(value)
        } else if let Some(v) = resolve_maybe_attribute_value_from_expected_element(
            Rc::clone(&element),
            &["data", "input"],
            "value",
        )
        .or_else(|| {
            resolve_maybe_attribute_value_from_expected_element(
                Rc::clone(&element),
                &["abbr"],
                "title",
            )
        }) {
            Some(v)
        } else if let Some(img) = Some(Rc::clone(&element))
            .filter(|element| &element.name == "img")
            .and_then(|elem| self.parse_into_image(elem, "src"))
        {
            return Ok(vec![img]);
        } else {
            Some(extract_text(
                Rc::clone(&element.node),
                self.base_url.clone(),
            )?)
        };

        if let Some(url) = value.and_then(|v| self.resolve_url(&v).ok()) {
            Ok(vec![PropertyValue::Url(url)])
        } else {
            Ok(Vec::default())
        }
    }

    fn resolve_implied_photo(
        &self,
        element: Rc<Element>,
        item: Rc<RefCell<Item>>,
    ) -> Option<PropertyValue> {
        let mf2_item = item.deref().borrow();
        let properties = mf2_item.properties.deref().borrow();
        if let Some(photo) = properties.get("photo").filter(|p| !p.is_empty()) {
            trace!("This item already has the 'photo' property as {:#?}, no need to resolve an implied one.", photo);
            return None;
        }

        // if img.h-x[src], then use the result of "parse an img element for src and alt" (see Sec.1.5) for photo
        if &element.name == "img" && element.has_attribute("src") {
            self.parse_into_image(Rc::clone(&element), "src")
        }
        // if object.h-x[data] then use data for photo
        else if &element.name == "object" && element.has_attribute("data") {
            self.parse_into_image(Rc::clone(&element), "data")
        }
        // if .h-x>img[src]:only-of-type:not[.h-*] then use the result of "parse an img element for src and alt" (see Sec.1.5) for photo
        // if .h-x>object[data]:only-of-type:not[.h-*] then use that object’s data for photo
        // if .h-x>:only-child:not[.h-*]>img[src]:only-of-type:not[.h-*], then use the result of "parse an img element for src and alt" (see Sec.1.5) for photo
        // if .h-x>:only-child:not[.h-*]>object[data]:only-of-type:not[.h-*], then use that object’s data for photo
        else if let Some((element, attribute)) =
            resolve_solo_of_type_of_solo_child(Rc::clone(&element), "img")
                .or_else(|| resolve_solo_of_type_of_solo_child(Rc::clone(&element), "object"))
                .or_else(|| resolve_solo_non_item_element(Rc::clone(&element), "img"))
                .or_else(|| resolve_solo_non_item_element(Rc::clone(&element), "object"))
                .or_else(|| resolve_only_child(Rc::clone(&element), &["img", "object"]))
                .and_then(|element| {
                    if &element.name == "img" && element.has_attribute("src") {
                        Some((element, "src"))
                    } else if &element.name == "object" && element.has_attribute("data") {
                        Some((element, "data"))
                    } else {
                        None
                    }
                })
        {
            self.parse_into_image(element, attribute)
        } else {
            None
        }
    }

    fn parse_into_image(
        &self,
        element: Rc<Element>,
        attribute_name: &str,
    ) -> Option<PropertyValue> {
        element
            .attribute(attribute_name)
            .and_then(|u| self.resolve_url(&u).ok())
            .and_then(|src| {
                if let Some(alt) = element.attribute("alt") {
                    Some(PropertyValue::Image(Image { src, alt }))
                } else {
                    Some(PropertyValue::Url(src))
                }
            })
    }

    fn parse_temporal_property(&self, element: Rc<Element>) -> Vec<PropertyValue> {
        if let Some(timestamp) = value_class::parse(
            Rc::clone(&element.node),
            self.base_url.clone(),
            value_class::TypeHint::Temporal,
        ) {
            return vec![timestamp];
        }

        let maybe_text = if ["time", "ins", "del"].contains(&element.name.as_str())
            && element.has_attribute("datetime")
        {
            element.attribute("datetime")
        } else if ["abbr", "input"].contains(&element.name.as_str())
            && element.has_attribute("title")
        {
            element.attribute("title")
        } else if ["data", "input"].contains(&element.name.as_str())
            && element.has_attribute("value")
        {
            element.attribute("value")
        } else {
            extract_only_text(Rc::clone(&element.node), self.base_url.clone())
                .ok()
                .filter(|v| !v.is_empty())
                .map(|v| v.trim().to_string())
        };

        if let Some(text) = maybe_text {
            trace!("Resolved temporal information: {:?}.", text);
            match value_class::parse_temporal_value(&text) {
                Ok(tmp) => vec![PropertyValue::Temporal(tmp)],
                Err(e) => {
                    error!("Failed to parse {:?} as temporal info {:#?}", text, e);
                    vec![]
                }
            }
        } else {
            trace!("No temporal information could be resolved.");
            vec![]
        }
    }
    fn resolve_implied_name(
        &self,
        element: Rc<Element>,
        item: Rc<RefCell<Item>>,
    ) -> Result<Option<String>, crate::Error> {
        let mf2_item = item.deref().borrow();
        let properties = mf2_item.properties.deref().borrow();

        let has_existing_name = properties.get("name").filter(|n| !n.is_empty()).is_some();
        let has_other_plain_or_html_properties = properties.values().flatten().any(|v| match v {
        PropertyValue::Image { .. } | PropertyValue::Plain(_) | PropertyValue::Fragment { .. } => { debug!("Failing to determine implied name due to the existence of {:#?} as a value on {:#?}", v, item); true },
        _ => false,
    });

        if has_existing_name {
            trace!(
            "The property 'name' is already defined on this item as {:#?}; not attempting to imply 'name'.",
            properties.get("name")
        );
            return Ok(None);
        } else if has_other_plain_or_html_properties {
            trace!(
            "The item {:#?} with {:#?} has existing textual properties associated; not attempting to imply 'name'.",
             item, element
        );
            return Ok(None);
        } else {
            trace!(
                "Attempting to resolve the implied name property from {:#?}",
                element
            );
        }

        if let Some(value) = resolve_attribute_value_from_expected_property_element(
            Rc::clone(&element),
            &["img", "area"],
            "alt",
        ) {
            trace!("poooint 1 {:#?}", value);
            Ok(Some(value))
        } else if let Some(value) = resolve_attribute_value_from_expected_property_element(
            Rc::clone(&element),
            &["abbr"],
            "title",
        ) {
            trace!("poooint 2 {:#?}", value);
            Ok(Some(value))
        } else if let Some(value) = resolve_solo_non_item_element(Rc::clone(&element), "img")
            .or_else(|| resolve_solo_non_item_element(Rc::clone(&element), "area"))
            .and_then(|element| {
                resolve_attribute_value_from_expected_element(element, &["img", "area"], "alt")
            })
            .or_else(|| {
                resolve_solo_non_item_element(Rc::clone(&element), "abbr").and_then(|elem| {
                    resolve_attribute_value_from_expected_element(elem, &["abbr"], "title")
                })
            })
            .filter(|v| !v.is_empty())
        {
            trace!("poooint 3 {:#?}", value);
            Ok(Some(value))
        } else if let Some(value) =
            resolve_solo_child_element_of_solo_child(Rc::clone(&element), "img")
                .or_else(|| resolve_solo_child_element_of_solo_child(Rc::clone(&element), "area"))
                .or_else(|| resolve_solo_child_element_of_solo_child(Rc::clone(&element), "abbr"))
                .and_then(|element| {
                    if ["img", "area"].contains(&element.name.as_str())
                        && element.has_attribute("alt")
                    {
                        element.attribute("alt")
                    } else if ["abbr"].contains(&element.name.as_str())
                        && element.has_attribute("title")
                    {
                        element.attribute("title")
                    } else {
                        None
                    }
                })
                .filter(|v| !v.is_empty())
        {
            trace!("poooint 4 {:#?}", value);
            Ok(Some(value))
        } else {
            let value = Some(extract_only_text(
                Rc::clone(&element.node),
                self.base_url.clone(),
            )?)
            .filter(|v| !v.is_empty())
            .map(|v| v.trim().to_string());
            trace!("poooint 5 {:#?}", value);
            Ok(value)
        }
    }
}

fn resolve_only_child(element: Rc<Element>, tag_names: &[&str]) -> Option<Rc<Element>> {
    tag_names.iter().find_map(|tag_name| {
        let elems = element
            .deref()
            .node
            .children
            .borrow()
            .iter()
            .filter_map(|node| Element::try_from(Rc::clone(node)).ok().map(Rc::new))
            .filter(|element| {
                !["script", "style", "template"].contains(&element.name.as_str())
                    && &element.name == tag_name
            })
            .collect::<Vec<_>>();

        elems.first().filter(|_| elems.len() == 1).map(Rc::clone)
    })
}

fn resolve_solo_child_element_of_solo_child(
    element: Rc<Element>,
    expected_tag_name: &str,
) -> Option<Rc<Element>> {
    resolve_single_child_element(Rc::clone(&element)).and_then(|child| {
        resolve_single_child_element(Rc::clone(&child))
            .filter(|element| &element.name == expected_tag_name)
    })
}

fn resolve_solo_of_type_of_solo_child(
    element: Rc<Element>,
    expected_tag_name: &str,
) -> Option<Rc<Element>> {
    resolve_single_child_element(Rc::clone(&element)).and_then(|child| {
        child
            .node
            .children
            .borrow()
            .deref()
            .iter()
            .filter_map(|n| Element::try_from(Rc::clone(n)).ok().map(Rc::new))
            .filter(|element| &element.name == expected_tag_name)
            .collect::<Vec<_>>()
            .first()
            .cloned()
    })
}

fn resolve_single_child_element(element: Rc<Element>) -> Option<Rc<Element>> {
    let items = element
        .deref()
        .node
        .children
        .borrow()
        .iter()
        .filter_map(|node| Element::try_from(Rc::clone(node)).ok().map(Rc::new))
        .filter(|element| !["script", "style", "template"].contains(&element.name.as_str()))
        .collect::<Vec<_>>();

    if items.len() == 1 {
        items.first().cloned()
    } else {
        None
    }
}

fn resolve_solo_non_item_element(
    element: Rc<Element>,
    expected_tag_name: &str,
) -> Option<Rc<Element>> {
    resolve_single_child_element(Rc::clone(&element))
        .filter(|element| &element.name == expected_tag_name)
        .filter(|element| !element.is_microformat_item())
}

fn resolve_implied_url(element: Rc<Element>, item: Rc<RefCell<Item>>) -> Option<String> {
    trace!("Parsing the implied value of 'url' for this element.");

    let props = &item.deref().borrow().properties;

    if let Some(u) = props.deref().borrow().get("url").filter(|u| !u.is_empty()) {
        trace!("Not implying a URL since 'url' is set to {:#?} already.", u);
        return None;
    }

    if props.deref().borrow().values().flatten().any(|value| {
        if let PropertyValue::Url(_) = value {
            true
        } else if let PropertyValue::Item(_) = value {
            true
        } else {
            false
        }
    }) {
        trace!("This item contains a URL-based property or a nested item; bailng out.");
        return None;
    }
    if let Some(value) = resolve_maybe_attribute_value_from_expected_element(
        Rc::clone(&element),
        &["a", "area"],
        "href",
    )
    .filter(|_| element.is_microformat_item())
    {
        Some(value)
    } else if let Some(value) = resolve_solo_non_item_element(Rc::clone(&element), "a")
        .or_else(|| resolve_solo_non_item_element(Rc::clone(&element), "area"))
        .or_else(|| resolve_solo_child_element_of_solo_child(Rc::clone(&element), "a"))
        .or_else(|| resolve_solo_child_element_of_solo_child(Rc::clone(&element), "area"))
        .or_else(|| resolve_only_child(Rc::clone(&element), &["a", "area"]))
        .and_then(|elem| elem.maybe_attribute("href"))
    {
        Some(value)
    } else {
        None
    }
}

fn extract_html(node: Rc<Node>, base_url: Url) -> Result<String, crate::Error> {
    let mut serializer = HtmlExtractionSerializer::new(base_url);
    node.serialize(&mut serializer, TraversalScope::IncludeNode)
        .map_err(crate::Error::IO)
        .and_then(|()| serializer.to_string())
}

#[test]
fn extract_html_test() {
    crate::test::enable_logging();
    let html = "<html><head></head><body>wow</body></html>";
    let dom_result = parse_document(RcDom::default(), ParseOpts::default())
        .from_utf8()
        .read_from(&mut html.as_bytes());

    assert!(dom_result.is_ok());

    let dom = dom_result.unwrap().document;

    let obtained_html = extract_html_from_children(dom, "https://example.com".parse().unwrap());

    assert_eq!(obtained_html, Ok(html.to_owned()));
}

fn extract_html_from_children(node: Rc<Node>, base_url: Url) -> Result<String, crate::Error> {
    node.children
        .borrow()
        .iter()
        .try_fold(String::default(), |mut acc, node| {
            match extract_html(Rc::clone(node), base_url.clone()) {
                Ok(html) => {
                    acc.push_str(&html);
                    Ok(acc)
                }
                Err(e) => Err(e),
            }
        })
}
use std::io::Write;

#[derive(Default)]
struct ElemInfo {
    html_name: Option<LocalName>,
    ignore_children: bool,
}
struct HtmlExtractionSerializer {
    pub writer: BufWriter<Vec<u8>>,
    base_url: Url,
    stack: Vec<ElemInfo>,
}

fn tagname(name: &QualName) -> LocalName {
    name.local.clone()
}

impl HtmlExtractionSerializer {
    pub fn new(url: Url) -> Self {
        let writer = BufWriter::new(Vec::default());
        Self {
            writer,
            base_url: url,
            stack: vec![ElemInfo {
                html_name: None,
                ignore_children: false,
            }],
        }
    }

    pub fn to_string(&self) -> Result<String, crate::Error> {
        String::from_utf8(self.writer.buffer().to_vec()).map_err(crate::Error::Utf8)
    }

    fn parent(&mut self) -> &mut ElemInfo {
        if self.stack.is_empty() {
            warn!("ElemInfo stack empty, creating new parent");
            self.stack.push(Default::default());
        }
        self.stack.last_mut().unwrap()
    }

    fn write_escaped(&mut self, text: &str, attr_mode: bool) -> io::Result<()> {
        for c in text.chars() {
            match c {
                '&' => self.writer.write_all(b"&amp;"),
                '\u{00A0}' => self.writer.write_all(b"&nbsp;"),
                '"' if attr_mode => self.writer.write_all(b"&quot;"),
                '<' if !attr_mode => self.writer.write_all(b"&lt;"),
                '>' if !attr_mode => self.writer.write_all(b"&gt;"),
                c => self.writer.write_fmt(format_args!("{}", c)),
            }?;
        }
        Ok(())
    }
}

fn expand_attribute_value(attribute_name: &str, attribute_value: &str, base_url: &Url) -> String {
    if attribute_name == "src" || attribute_name == "href" {
        if attribute_value.is_empty() {
            base_url.to_string()
        } else {
            base_url
                .join(attribute_value.clone())
                .map(|u| u.to_string())
                .unwrap_or(attribute_value.to_string())
        }
    } else {
        attribute_value.to_string()
    }
}

impl html5ever::serialize::Serializer for HtmlExtractionSerializer {
    fn start_elem<'a, AttrIter>(
        &mut self,
        name: html5ever::QualName,
        attrs: AttrIter,
    ) -> io::Result<()>
    where
        AttrIter: Iterator<Item = html5ever::serialize::AttrRef<'a>>,
    {
        if self.parent().ignore_children {
            self.stack.push(ElemInfo {
                html_name: Some(name.local.clone()),
                ignore_children: true,
            });
            return Ok(());
        }

        self.writer.write_all(b"<")?;
        self.writer.write_all(tagname(&name).as_bytes())?;
        for (name, value) in attrs {
            self.writer.write_all(b" ")?;

            self.writer.write_all(name.local.as_bytes())?;
            self.writer.write_all(b"=\"")?;

            let actual_value =
                expand_attribute_value(name.local.to_string().as_str(), value, &self.base_url);

            self.write_escaped(&actual_value, true)?;
            self.writer.write_all(b"\"")?;
        }
        self.writer.write_all(b">")?;

        let ignore_children = match name.local {
            local_name!("area")
            | local_name!("base")
            | local_name!("basefont")
            | local_name!("bgsound")
            | local_name!("br")
            | local_name!("col")
            | local_name!("embed")
            | local_name!("frame")
            | local_name!("hr")
            | local_name!("img")
            | local_name!("input")
            | local_name!("keygen")
            | local_name!("link")
            | local_name!("meta")
            | local_name!("param")
            | local_name!("source")
            | local_name!("track")
            | local_name!("wbr") => true,
            _ => false,
        };

        self.stack.push(ElemInfo {
            html_name: Some(name.local.clone()),
            ignore_children,
        });

        Ok(())
    }

    fn end_elem(&mut self, name: html5ever::QualName) -> io::Result<()> {
        let info = match self.stack.pop() {
            Some(info) => info,
            _ => panic!("no ElemInfo"),
        };
        if info.ignore_children {
            return Ok(());
        }

        self.writer.write_all(b"</")?;
        self.writer.write_all(tagname(&name).as_bytes())?;
        self.writer.write_all(b">")
    }

    fn write_text(&mut self, text: &str) -> io::Result<()> {
        match self.parent().html_name {
            Some(local_name!("iframe"))
            | Some(local_name!("noembed"))
            | Some(local_name!("noframes"))
            | Some(local_name!("plaintext")) => Ok(()),
            _ => self.write_escaped(text, false),
        }
    }

    fn write_comment(&mut self, _text: &str) -> io::Result<()> {
        Ok(())
    }

    fn write_doctype(&mut self, _name: &str) -> io::Result<()> {
        Ok(())
    }

    fn write_processing_instruction(&mut self, _target: &str, _data: &str) -> io::Result<()> {
        Ok(())
    }
}

struct PlainTextExtractionSerializer {
    pub writer: BufWriter<Vec<u8>>,
    base_url: Url,
    ignore_text: bool,
    swap_img_with_src: bool,
}

impl PlainTextExtractionSerializer {
    pub fn new(base_url: Url) -> Self {
        let writer = BufWriter::new(Vec::default());
        Self {
            writer,
            ignore_text: false,
            swap_img_with_src: true,
            base_url,
        }
    }

    pub fn to_string(&self) -> Result<String, crate::Error> {
        String::from_utf8(self.writer.buffer().to_vec()).map_err(crate::Error::Utf8)
    }
}

impl html5ever::serialize::Serializer for PlainTextExtractionSerializer {
    fn start_elem<'a, AttrIter>(
        &mut self,
        name: html5ever::QualName,
        mut attrs: AttrIter,
    ) -> io::Result<()>
    where
        AttrIter: Iterator<Item = html5ever::serialize::AttrRef<'a>>,
    {
        if ["script", "style", "template"]
            .contains(&name.local.to_ascii_lowercase().to_string().as_str())
        {
            self.ignore_text = true;
            Ok(())
        } else if name.local.to_ascii_lowercase().to_string().as_str() == "img" {
            let lookup_attrs = if self.swap_img_with_src {
                vec!["src", "alt"]
            } else {
                vec!["alt"]
            };
            let attr_opt = attrs
                .find(|(name, _)| {
                    lookup_attrs.contains(&name.local.to_ascii_lowercase().to_string().as_str())
                })
                .map(|(name, value)| {
                    if &name.local == "src" {
                        format!(
                            " {} ",
                            expand_attribute_value(
                                name.local.to_string().as_str(),
                                value,
                                &self.base_url
                            )
                        )
                    } else {
                        value.trim().to_string()
                    }
                })
                .filter(|v| {
                    if self.swap_img_with_src {
                        true
                    } else {
                        !v.is_empty()
                    }
                });

            if let Some(attr_string) = attr_opt {
                self.write_text(&attr_string)
            } else {
                Ok(())
            }
        } else {
            Ok(())
        }
    }

    fn end_elem(&mut self, _name: html5ever::QualName) -> io::Result<()> {
        self.ignore_text = false;
        Ok(())
    }

    fn write_text(&mut self, text: &str) -> io::Result<()> {
        if !self.ignore_text {
            self.writer.write(text.as_bytes()).and(Ok(()))
        } else {
            Ok(())
        }
    }

    fn write_comment(&mut self, _text: &str) -> io::Result<()> {
        Ok(())
    }

    fn write_doctype(&mut self, _name: &str) -> io::Result<()> {
        Ok(())
    }

    fn write_processing_instruction(&mut self, _target: &str, _data: &str) -> io::Result<()> {
        Ok(())
    }
}

fn extract_only_text(parent_node: Rc<Node>, base_url: Url) -> Result<String, crate::Error> {
    let mut serializer = PlainTextExtractionSerializer::new(base_url);
    serializer.swap_img_with_src = false;
    parent_node
        .serialize(&mut serializer, TraversalScope::IncludeNode)
        .map_err(crate::Error::IO)
        .and_then(|()| serializer.to_string())
}

pub fn extract_text(parent_node: Rc<Node>, base_url: Url) -> Result<String, crate::Error> {
    let mut serializer = PlainTextExtractionSerializer::new(base_url);
    parent_node
        .serialize(&mut serializer, TraversalScope::IncludeNode)
        .map_err(crate::Error::IO)
        .and_then(|()| serializer.to_string())
}

#[test]
fn extract_text_test() {
    crate::test::enable_logging();
    let html = "<html><head></head><body>Sometimes, I want to just <b>jump</b> and <em>run</em>.</body></html>";
    let dom_result = parse_document(RcDom::default(), ParseOpts::default())
        .from_utf8()
        .read_from(&mut html.as_bytes());

    assert!(dom_result.is_ok());

    let dom = dom_result.unwrap().document;

    let obtained_text = dom
        .children
        .borrow()
        .iter()
        .filter_map(|child| {
            extract_only_text(Rc::clone(child), "https://indieweb.org".parse().unwrap()).ok()
        })
        .collect::<Vec<_>>()
        .join("");

    assert_eq!(
        obtained_text,
        "Sometimes, I want to just jump and run.".to_string()
    );
}

fn resolve_maybe_attribute_value_from_expected_element(
    element: Rc<Element>,
    tag_names: &[&str],
    attribute_name: &str,
) -> Option<String> {
    if !tag_names.contains(&element.name.as_str()) {
        return None;
    }

    element.maybe_attribute(attribute_name)
}

fn resolve_attribute_value_from_expected_element(
    element: Rc<Element>,
    tag_names: &[&str],
    attribute_name: &str,
) -> Option<String> {
    if !tag_names.contains(&element.name.as_str()) {
        return None;
    }

    element.attribute(attribute_name)
}

fn resolve_attribute_value_from_expected_property_element(
    element: Rc<Element>,
    tag_names: &[&str],
    attribute_name: &str,
) -> Option<String> {
    if !tag_names.contains(&element.name.as_str()) {
        return None;
    }

    if !element.is_property_element() {
        return None;
    }

    element.attribute(attribute_name)
}

fn attribute_value(attrs: RefCell<Vec<Attribute>>, name: &str) -> Option<String> {
    attrs
        .borrow()
        .iter()
        .find(|attr| &attr.name.local.to_ascii_lowercase() == name)
        .map(|attr| attr.value.to_string())
}

#[test]
fn parser_parser_node() {
    use assert_json_diff::assert_json_eq;
    crate::test::enable_logging();
    let parser_html = r#"
    <html>
    <body>
        <div class="h-feed" id="main">
            <p class="p-summary">This feels pretty simple.</p>
            <div class="e-content">It's showtime, for <strong>real.</strong></div>
            <p>Last published <time class="dt-published" datetime="2022-02-01T17:38:31-05:00">last week</time></p>
            Written by <div class="u-author h-card"><a href="/u-author" class="u-url">Jacky</a></div> and <span class="p-author">PAuthor</span>
            <div class="h-cite"><a class="u-url" href="/post/1">Post</a></div>
            <div class="h-cite"><a class="u-url" href="/post/2">Post</a></div>
            <div class="h-cite"><a class="u-url" href="/post/3">Post</a></div>
            <div class="h-cite"><a class="u-url" href="/post/4">Post</a></div>
            <div class="h-cite"><a class="u-url" href="/post/5">Post</a></div>
        </div>
    </body>
    </html>
    "#;

    let parser_result = Parser::new(parser_html, "https://indieweb.org".parse().unwrap());
    assert_eq!(None, parser_result.as_ref().err());
    let mut parser = parser_result.unwrap();

    let parser_parsed_result = parser.parse();
    assert_eq!(None, parser_parsed_result.as_ref().err());
    let result = parser_parsed_result.unwrap();

    assert_json_eq!(
        serde_json::from_str::<Document>(
            r#"{
            "items": [
                {
                    "children": [
                        {
                            "type": ["h-cite"],
                            "properties": {
                                "url": ["https://indieweb.org/post/1"],
                                "name": ["Post"]
                            }
                        },
                        {
                            "type": ["h-cite"],
                            "properties": {
                                "url": ["https://indieweb.org/post/2"],
                                "name": ["Post"]
                            }
                        },
                        {
                            "type": ["h-cite"],
                            "properties": {
                                "url": ["https://indieweb.org/post/3"],
                                "name": ["Post"]
                            }
                        },
                        {
                            "type": ["h-cite"],
                            "properties": {
                                "url": ["https://indieweb.org/post/4"],
                                "name": ["Post"]
                            }
                        },
                        {
                            "type": ["h-cite"],
                            "properties": {
                                "url": ["https://indieweb.org/post/5"],
                                "name": ["Post"]
                            }
                        }
                    ],
                    "properties": {
                        "author": [
                            {
                                "type": ["h-card"],
                                "properties": {
                                    "url": ["https://indieweb.org/u-author"],
                                    "name": ["Jacky"]
                                },
                                "value": "https://indieweb.org/u-author"
                            },
                            "PAuthor"
                        ],
                        "content": [
                            {
                                "html": "It's showtime, for <strong>real.</strong>",
                                "value": "It's showtime, for real."
                            }
                        ],
                        "summary": ["This feels pretty simple."],
                        "published": ["2022-02-01T17:38:31-0500"]
                    },
                    "id": "main",
                    "type": ["h-feed"]
                }
            ],
            "rels": {},
            "rel-urls": {}
        }"#
        )
        .unwrap_or_default(),
        serde_json::to_value(result).unwrap_or_default()
    );
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
enum PropertyClass {
    /// Represents `h-`
    Root(String),
    /// Represents `p-`
    Plain(String),
    /// Represents `u-`
    Linked(String),
    /// Represents `dt-`
    Timestamp(String),
    /// Represents `e-`
    Hypertext(String),
}

impl PropertyClass {
    /// Extract property classes associated to the definition of a `crate::types::Class`.
    pub fn extract_root_classes(properties: Vec<Self>) -> Vec<Class> {
        properties
            .into_iter()
            .filter_map(|property| {
                if let Self::Root(class) = property {
                    Class::from_str(format!("h-{}", class).as_str()).ok()
                } else {
                    None
                }
            })
            .collect()
    }

    /// Determines if this class is a 'h-'
    pub fn is_root(&self) -> bool {
        if let Self::Root(_) = self {
            true
        } else {
            false
        }
    }

    /// Convert string into a list of PropertyClass.
    ///
    /// Converts a string representing a list of class values into a list of `PropertyClass` items
    /// to their matching property names.
    pub fn list_from_string(property_class_string: String) -> Vec<Self> {
        let mut classes = property_class_string
            .split_ascii_whitespace()
            .into_iter()
            .filter_map(|class_name| {
                RE_CLASS_NAME.captures(class_name).and_then(|cp| {
                    let prefix = cp.name("prefix").map(|s| s.as_str());
                    let name = cp.name("name").map(|s| s.as_str());

                    prefix.and_then(|prefix| name.map(|p| (prefix, p)))
                })
            })
            .map(|(prefix, name)| Self::from_prefix_and_name(&prefix, &name))
            .collect::<Vec<Self>>();

        classes.sort();
        classes.dedup();
        classes
    }

    pub fn from_prefix_and_name(prefix: &str, name: &str) -> Self {
        match prefix {
            "u" => Self::Linked(name.to_string()),
            "dt" => Self::Timestamp(name.to_string()),
            "e" => Self::Hypertext(name.to_string()),
            "h" => Self::Root(name.to_string()),
            _ => Self::Plain(name.to_string()),
        }
    }
}

pub mod value_class {
    use crate::types::temporal;

    use super::*;
    /// Takes in a temporal string and converts it into a potential `Temporal` value.
    pub fn parse_temporal_value(temporal_value: &str) -> Result<temporal::Value, crate::Error> {
        temporal::Value::from_str(temporal_value).map_err(crate::Error::Temporal)
    }

    fn resolve_value_elements(node: Rc<Node>) -> Vec<Element> {
        node.children
            .borrow()
            .iter()
            .filter_map(|node| Element::try_from(Rc::clone(node)).ok())
            .filter(|element| element.has_class("value") || element.has_class("value-title"))
            .collect::<Vec<_>>()
    }

    fn extract_values(elements: Vec<Element>, base_url: Url) -> Vec<String> {
        elements
            .into_iter()
            .filter_map(|element| {
                if element.has_class("value") {
                    if ["img", "area"].contains(&element.name.as_str()) {
                        element.attribute("alt")
                    } else if &element.name == "data" {
                        element.attribute("value").or_else(|| {
                            extract_only_text(Rc::clone(&element.node), base_url.clone()).ok()
                        })
                    } else if &element.name == "abbr" {
                        element.attribute("title").or_else(|| {
                            extract_only_text(Rc::clone(&element.node), base_url.clone()).ok()
                        })
                    } else if ["time", "ins", "del"].contains(&element.name.as_str()) {
                        element.attribute("datetime").or_else(|| {
                            extract_only_text(Rc::clone(&element.node), base_url.clone()).ok()
                        })
                    } else {
                        extract_only_text(Rc::clone(&element.node), base_url.clone()).ok()
                    }
                } else if element.has_class("value-title") {
                    element.attribute("title")
                } else {
                    None
                }
            })
            .filter(|v| !v.is_empty())
            .map(|v| v.trim().to_string())
            .collect::<Vec<String>>()
    }

    pub fn compose_temporal_value(values: Vec<String>) -> Option<temporal::Value> {
        if let Some(iso8601_value) = values
            .first()
            .map(|s| s.as_str())
            .and_then(|s| temporal::Stamp::from_iso8601(s).ok())
            .filter(|_| values.len() == 1)
        {
            trace!(
                "The provided value-class value was a timestamp; returning {:#?}",
                iso8601_value
            );
            Some(temporal::Value::Timestamp(iso8601_value))
        } else if let Ok(value) = temporal::Duration::from_str(&values.join("")) {
            Some(temporal::Value::Duration(value))
        } else if values.len() == 0 {
            trace!("No value was provided for temporal parsing.");
            None
        } else {
            trace!("Attempting to parse {:#?} into a temporal value.", values);
            let mut date = None;
            let mut time = None;
            let mut offset = None;

            for value_result in values.iter().cloned().map(|v| temporal::Stamp::parse(&v)) {
                if let Ok(value) = value_result {
                    if value.is_date() && date.is_none() {
                        date = value.as_date()
                    } else if value.is_time() && time.is_none() {
                        time = value.as_time();
                    } else if value.is_offset() && offset.is_none() {
                        offset = value.as_offset();
                    }
                }
            }

            time = if let Some(mut t) = time {
                if t.prefix.is_none() {
                    t.prefix = Some(' ');
                }
                Some(t)
            } else {
                time
            };

            temporal::Stamp::compose(date, time, offset).map(temporal::Value::Timestamp)
        }
    }

    #[test]
    fn compose_temporal_value_test() {
        assert_eq!(
            compose_temporal_value(vec!["2000-10-10".to_owned(), "10:00Z".to_owned()])
                .map(|t| t.to_string()),
            Some("2000-10-10 10:00Z".to_owned())
        );
        assert_eq!(
            compose_temporal_value(vec!["2000-100".to_owned(), "10:00Z".to_owned()])
                .map(|t| t.to_string()),
            Some("2000-100 10:00Z".to_owned())
        );
    }

    #[derive(Eq, PartialEq)]
    pub enum TypeHint {
        Plain,
        Temporal,
    }

    pub fn parse(node: Rc<Node>, base_url: Url, type_hint: TypeHint) -> Option<PropertyValue> {
        let values = extract_values(resolve_value_elements(Rc::clone(&node)), base_url);

        if values.is_empty() {
            trace!("Nothing was parsed during value-class parsing.",);
            None
        } else {
            trace!("Resolved {:#?} as the value-class result", values);
            if type_hint == TypeHint::Temporal {
                compose_temporal_value(values).map(PropertyValue::Temporal)
            } else {
                Some(PropertyValue::Plain(values.join("")))
            }
        }
    }
}

#[test]
fn property_class_list_from_string() {
    crate::test::enable_logging();
    assert_eq!(
        PropertyClass::list_from_string("p-name".to_owned()),
        vec![PropertyClass::Plain("name".to_owned())]
    );

    assert_eq!(
        PropertyClass::list_from_string("p-name zoom x-foo e-content".to_owned()),
        vec![
            PropertyClass::Plain("name".to_owned()),
            PropertyClass::Hypertext("content".to_owned())
        ]
    );
}
