lazy_static::lazy_static! {
    static ref RE_ISO8601_DATETIME_OFFSET: Regex = Regex::new(r"^(?P<year>\d{4})-?(?P<month>\d{2})-?(?P<day>\d{2})(?P<prefix>[tT])(?P<hour>\d{1,2}):?(?P<minute>\d{2})?(:?(?P<second>\d{2})(\.?(?P<nano>\d{1,10}))?)?(?P<tz>[Z ]?([+-]\d{1,2}(?P<offset_colon>:?)(\d{2})?)?)?$").unwrap();
    static ref RE_DATE_YMD: Regex = Regex::new(r#"^(?P<year>\d{4})-(?P<month>\d{1,2})?(-(?P<day>\d{1,2}))?$"#).unwrap();
    static ref RE_DATE_YO: Regex = Regex::new(r#"^(?P<year>\d{4})-(?P<ordinal>\d{3})?$"#).unwrap();
    static ref RE_TIME_HMS_OFFSET_MERIDAN: Regex = Regex::new(r#"^(?P<prefix>[\stT]?)?(?P<hour>\d{1,2})[ :]?(?P<minute>\d{2})?:?((?P<second>\d{2})(\.?(?P<nano>\d{1,10}))?)?:?\s?(?P<meridan>[apAP]\.?[mM]\.?)?\s?(?P<tz>[zZ]|[+-]\d{1,2}:?(\d{2})?)?$"#).unwrap();
    static ref RE_TIME_OFFSET: Regex = Regex::new(r#"^(?P<tz>[zZ]|[+-]\d{1,2}(:?\d{2})?)$"#).unwrap();
    static ref RE_OFFSET_RANGE: Regex = Regex::new(r"^(?P<hour>\d{1,2})(?P<offset_colon>:?)?(?P<minute>\d{2})?$").unwrap();
    static ref RE_DURATION: Regex = Regex::new(r#"^P(?P<period>((?P<year>\d{1,2})[Yy])?-?(((?P<month>\d{1,2})[Mm])-?)?((?P<week>\d{1,2})[Ww])?-?((?P<day>\d{1,2})[dd])?)?T?(?P<time>((?P<hour>\d{1,2})[Hh]?):?((?P<minute>:?\d{1,2})[Mm]?)?:?((?P<second>\d{1,2})[Ss]?)?)?$"#).unwrap();
}
use std::num::ParseIntError;

use chrono::{
    format::{DelayedFormat, StrftimeItems},
    Timelike,
};
use serde::Deserialize;

use super::*;

#[test]
fn regex_iso8601_datetime_offset() {
    assert!(RE_ISO8601_DATETIME_OFFSET.is_match("2022-02-07T19:22:27+00:00"));
    assert!(RE_ISO8601_DATETIME_OFFSET.is_match("2022-02-07T19:22:27.100+00:00"));
    assert!(RE_ISO8601_DATETIME_OFFSET.is_match("2022-02-07T19:22:27Z"));
    assert!(RE_ISO8601_DATETIME_OFFSET.is_match("20220207T192227Z"));
}

#[derive(thiserror::Error, Debug, PartialEq, Eq)]
pub enum Error {
    #[error("The offset {0} was invalid.")]
    InvalidOffset(String),
    #[error("The time {0} was invalid.")]
    InvalidTime(String),
    #[error("The date {0} was invalid.")]
    InvalidDate(String),
    #[error("The value {0} could not be parsed as a ISO8601/RFC2282 datetime string.")]
    NonISO8601DateTime(String),
    #[error("The value {0} could not be parsed as a datetime, date, time or offset.")]
    CompletelyInvalid(String),
    #[error("The time {0} is missing an hour value.")]
    MissingHour(String),
    #[error("The time {0} is missing an month value.")]
    MissingMonth(String),
    #[error("The time {0} is missing an dayvalue.")]
    MissingDay(String),
    #[error("The oridinal date {0} is missing an year value.")]
    MissingYear(String),
    #[error("The ordinal date {0} is missing an day value.")]
    MissingOrdinalDay(String),
    #[error("Could not parsed as an integer: {0}")]
    Integer(#[from] ParseIntError),
    #[error("Failed to validate the value as temporal data: {0}")]
    Parse(#[from] chrono::ParseError),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Offset {
    pub data: FixedOffset,
    with_minutes: bool,
}

impl PartialOrd for Offset {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.data.to_string().partial_cmp(&other.data.to_string())
    }
}

impl Ord for Offset {
    fn cmp(&self, other: &Self) -> Ordering {
        self.data.to_string().cmp(&other.data.to_string())
    }
}

impl ToString for Offset {
    fn to_string(&self) -> String {
        if self.data.utc_minus_local() == 0 {
            "Z".to_string()
        } else if self.with_minutes {
            let items = StrftimeItems::new("%z");
            DelayedFormat::new_with_offset(None, None, &self.data, items).to_string()
        } else {
            let seconds = self.data.utc_minus_local() as u32;
            let extra_minute = (seconds / 60) % 60;
            let hours = (seconds - (extra_minute * 60)) / 3600;
            format!(
                "{}{:0>2}",
                if self.data.utc_minus_local() > 0 {
                    "-"
                } else {
                    "+"
                },
                hours
            )
        }
    }
}

impl FromStr for Offset {
    type Err = Error;

    /// Converts the provided string into a representation of an offset.
    ///
    /// ```
    /// # use std::str::FromStr;
    /// # use microformats::types::temporal::Offset;
    ///
    /// assert_eq!(Offset::from_str("Z").map(|o| o.to_string()), Ok("Z".to_owned()));
    /// assert_eq!(Offset::from_str("+1:11").map(|o| o.to_string()), Ok("+0111".to_owned()));
    /// assert_eq!(Offset::from_str("-1:11").map(|o| o.to_string()), Ok("-0111".to_owned()));
    /// ```
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        trace!("Attempting to parse the offset of {:?}", s);
        let symbol_char = s.to_ascii_lowercase().chars().next();
        let (offset_secs, with_minutes) = s
            .get(1..)
            .and_then(|offset| RE_OFFSET_RANGE.captures(offset))
            .map(|captures| {
                let hour: i32 = captures
                    .name("hour")
                    .and_then(|v| v.as_str().parse().ok())
                    .unwrap_or_default();
                let (minute, has_minutes): (i32, bool) = (
                    captures
                        .name("minute")
                        .and_then(|v| v.as_str().parse().ok())
                        .unwrap_or_default(),
                    captures.name("minute").is_some(),
                );

                (hour * 60 * 60 + minute * 60, has_minutes)
            })
            .unwrap_or_default();

        if symbol_char == Some('z') {
            Ok(Self {
                data: FixedOffset::west(0),
                with_minutes,
            })
        } else if let Some('-') = symbol_char {
            FixedOffset::west_opt(offset_secs)
                .map(|data| Self { data, with_minutes })
                .ok_or_else(|| Error::InvalidOffset(s.to_string()))
        } else if let Some('+') = symbol_char {
            FixedOffset::east_opt(offset_secs)
                .map(|data| Self { data, with_minutes })
                .ok_or_else(|| Error::InvalidOffset(s.to_string()))
        } else {
            Err(Self::Err::InvalidOffset(s.to_string()))
        }
    }
}

impl Offset {
    fn from_offset(data: FixedOffset) -> Self {
        Self {
            data,
            with_minutes: true,
        }
    }
}

#[test]
fn offset_to_string() {
    assert_eq!(
        Offset::from_str("-0800").map(|o| o.to_string()),
        Ok("-0800".to_owned())
    );
    assert_eq!(
        Offset::from_str("-08:00").map(|o| o.to_string()),
        Ok("-0800".to_owned())
    );
    assert_eq!(
        Offset::from_str("-08").map(|o| o.to_string()),
        Ok("-08".to_owned())
    );
    assert_eq!(
        Offset::from_str("+00").map(|o| o.to_string()),
        Ok("Z".to_owned())
    );
    assert_eq!(
        Offset::from_str("+00:00").map(|o| o.to_string()),
        Ok("Z".to_owned())
    );
    assert_eq!(
        Offset::from_str("-00:00").map(|o| o.to_string()),
        Ok("Z".to_owned())
    );
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Time {
    pub data: NaiveTime,
    pub offset: Option<Offset>,
    pub has_seconds: bool,
    pub prefix: Option<char>,
}

impl Time {
    pub fn with_offset(&self, offset: Option<Offset>) -> Self {
        trace!("Trading {:#?} for {:#?}", self.offset, offset);
        let mut other = self.clone();
        other.offset = offset;
        other
    }

    pub fn from_time(
        data: NaiveTime,
        has_seconds: bool,
        prefix: Option<char>,
        offset: Option<Offset>,
    ) -> Self {
        Self {
            data,
            has_seconds,
            prefix,
            offset,
        }
    }
}

impl ToString for Time {
    fn to_string(&self) -> String {
        let mut format_items = vec![];
        if let Some(prefix) = self.prefix {
            format_items.push(prefix.to_string());
        }

        format_items.push(if self.has_seconds {
            "%T%.f".to_owned()
        } else {
            "%R".to_owned()
        });

        let format_str = format_items.join("");
        let items = StrftimeItems::new(&format_str);

        let time_string = DelayedFormat::new(None, Some(self.data), items).to_string();
        let offset_string = self.offset.as_ref().map(|o| o.to_string());

        vec![time_string, offset_string.unwrap_or_default()].join("")
    }
}

#[test]
fn time_to_string() {
    assert_eq!(
        Time::from_str("1 PM").map(|t| t.to_string()),
        Ok("13:00".to_owned())
    );
    assert_eq!(
        Time::from_str("1:30:10.3020 PM").map(|t| t.to_string()),
        Ok("13:30:10.302".to_owned())
    );

    assert_eq!(
        Time::from_str("T1 PM").map(|t| t.to_string()),
        Ok("T13:00".to_owned())
    );
    assert_eq!(
        Time::from_str(" 1 PM").map(|t| t.to_string()),
        Ok(" 13:00".to_owned())
    );
}

impl FromStr for Time {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        trace!("Attempting to parse {:?} as a time (and offset) value", s);
        RE_TIME_HMS_OFFSET_MERIDAN
            .captures(s)
            .ok_or_else(|| Self::Err::InvalidTime(s.to_string()))
            .and_then(|params| {
                let has_seconds = params.name("second").is_some();
                let prefix = params
                    .name("prefix")
                    .and_then(|m| m.as_str().chars().next());
                let mut hour = params
                    .name("hour")
                    .map(|m| m.as_str())
                    .ok_or_else(|| Self::Err::MissingHour(s.to_string()))?
                    .parse()
                    .map_err(Self::Err::Integer)?;
                let min = params
                    .name("minute")
                    .map(|m| m.as_str())
                    .unwrap_or("0")
                    .parse()
                    .map_err(Self::Err::Integer)?;
                let second = params
                    .name("second")
                    .map(|m| m.as_str())
                    .unwrap_or("0")
                    .parse()
                    .map_err(Self::Err::Integer)?;
                let nano = params
                    .name("nano")
                    .map(|m| format!("{:0<9}", m.as_str()))
                    .unwrap_or_else(|| "0".to_string())
                    .parse()
                    .map_err(Self::Err::Integer)?;

                let offset = params
                    .name("tz")
                    .and_then(|m| Offset::from_str(m.as_str()).ok());

                if Some("pm".to_string())
                    == params
                        .name("meridan")
                        .map(|m| m.as_str().to_ascii_lowercase().replace(".", ""))
                    && hour < 12
                {
                    hour += 12;
                }

                NaiveTime::from_hms_nano_opt(hour, min, second, nano)
                    .map(|data| Self {
                        data,
                        offset,
                        has_seconds,
                        prefix,
                    })
                    .ok_or_else(|| Self::Err::InvalidTime(s.to_owned()))
            })
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Date {
    pub data: NaiveDate,
    pub ordinal: bool,
    pub has_day: bool,
}

impl Date {
    fn from_date(data: NaiveDate, ordinal: bool, has_day: bool) -> Self {
        Self {
            data,
            ordinal,
            has_day,
        }
    }
}

impl ToString for Date {
    fn to_string(&self) -> String {
        if self.ordinal {
            self.data.format("%Y-%j").to_string()
        } else if !self.has_day {
            self.data.format("%Y-%m").to_string()
        } else {
            self.data.format("%F").to_string()
        }
    }
}

impl FromStr for Date {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let result = if let Some(parts) = RE_DATE_YO.captures(s) {
            let year = parts
                .name("year")
                .and_then(|v| v.as_str().parse().ok())
                .ok_or_else(|| Error::MissingYear(s.to_string()))?;
            let ordinal = parts
                .name("ordinal")
                .and_then(|v| v.as_str().parse().ok())
                .ok_or_else(|| Error::MissingOrdinalDay(s.to_string()))?;

            trace!("{} was parsed as YYYY-DDD.", s);
            Ok((NaiveDate::from_yo(year, ordinal), false))
        } else if let Some(parts) = RE_DATE_YMD.captures(s) {
            trace!("Attempting to parse as YYYY-MM-DD.");
            parts
                .name("year")
                .ok_or_else(|| Error::MissingYear(s.to_string()))
                .or_else(|_| {
                    parts
                        .name("month")
                        .ok_or_else(|| Error::MissingMonth(s.to_string()))
                })?;

            let has_day = parts.name("day").is_some();
            let adjusted_s = if has_day {
                s.to_string()
            } else {
                format!("{}-01", s)
            };

            NaiveDate::parse_from_str(&adjusted_s, "%Y-%0m-%0d")
                .map_err(Self::Err::Parse)
                .map(|d| (d, has_day))
        } else {
            trace!("{} didn't match an ordinal or cardnial date.", s);
            Err(Self::Err::InvalidDate(s.to_string()))
        };

        result.map(|(data, has_day)| Self {
            data,
            ordinal: RE_DATE_YO.is_match(s),
            has_day,
        })
    }
}

#[test]
fn date_to_string() {
    crate::test::enable_logging();
    assert_eq!(
        Date::from_str("2013-034").map(|s| s.to_string()),
        Ok("2013-034".to_owned())
    );
    assert_eq!(
        Date::from_str("2013-03-01").map(|s| s.to_string()),
        Ok("2013-03-01".to_owned())
    );
}

#[derive(Default, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Stamp {
    pub time: Option<Time>,
    pub date: Option<Date>,
    pub offset: Option<Offset>,
    was_iso8601: bool,
}

impl std::fmt::Display for Stamp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let v = [
            self.date.as_ref().map(|s| s.to_string()),
            self.time
                .as_ref()
                .map(|t| {
                    if self.offset.is_some() {
                        t.with_offset(self.offset.clone())
                    } else {
                        t.clone()
                    }
                })
                .map(|t| t.to_string()),
        ]
        .iter()
        .filter_map(|o| o.clone())
        .collect::<Vec<_>>()
        .join("");

        f.write_str(&v)
    }
}

impl std::fmt::Debug for Stamp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Stamp")
            .field("value", &self.to_string())
            .field("was_iso8601", &self.was_iso8601)
            .finish()
    }
}

impl Stamp {
    pub fn from_iso8601(temporal_value: &str) -> Result<Self, Error> {
        if let Some(components) = RE_ISO8601_DATETIME_OFFSET.captures(temporal_value) {
            let prefix = components
                .name("prefix")
                .map(|p| p.as_str())
                .and_then(|c| c.chars().next());
            trace!(
                "This matches (kinda) our ISO8601/RFC3339 regex using a {:?} prefix!",
                prefix
            );
            DateTime::parse_from_rfc3339(temporal_value)
                .map_err(Error::Parse)
                .map(|dt| {
                    trace!("Parsing {:#?} as a stamp", dt);
                    Self {
                        date: Some(Date::from_date(dt.date().naive_local(), false, true)),
                        time: Some(Time::from_time(dt.time(), true, prefix, None)).map(|mut t| {
                            t.prefix = prefix;
                            t
                        }),
                        offset: Some(Offset::from_offset(*dt.offset())),
                        was_iso8601: true,
                    }
                })
                .or_else(|_| {
                    trace!(
                        "Manually constructing this stamp with the {:?} prefix.",
                        prefix
                    );
                    let mut parts = temporal_value
                        .splitn(2, prefix.unwrap_or('T'))
                        .map(|s| s.to_owned());

                    let date = Date::from_str(&parts.next().unwrap())?;
                    let time = Time::from_str(&parts.next().unwrap())?;

                    Stamp::compose(Some(date), Some(time), None)
                        .map(|mut s| {
                            s.time.as_mut().map(|mut t| {
                                t.prefix = prefix;
                                t
                            });
                            s
                        })
                        .ok_or_else(|| Error::CompletelyInvalid(temporal_value.to_string()))
                })
        } else {
            trace!(
                "The value {:?} didn't match our expected ISO8601 regex.",
                temporal_value
            );
            Err(Error::NonISO8601DateTime(temporal_value.to_string()))
        }
    }

    pub fn parse(temporal_value: &str) -> Result<Self, Error> {
        let santizied_temporal_value = temporal_value.trim();
        let values = santizied_temporal_value
            .split(' ')
            .map(|t| t.to_string())
            .collect::<Vec<_>>();

        trace!(
            "Attempting to parse {:?} as temporal information (parts {:#?}.",
            santizied_temporal_value,
            values
        );
        let iso = Self::from_iso8601(santizied_temporal_value);

        // Match against the whole string.
        if iso.is_ok() {
            trace!("Loosely parsing a ISO8601 value.");
            iso
        } else if RE_TIME_OFFSET.is_match(santizied_temporal_value) {
            trace!("Parsing out an offset value.");
            Offset::from_str(santizied_temporal_value).map(|offset| offset.into())
        } else if RE_DATE_YO.is_match(santizied_temporal_value)
            || RE_DATE_YMD.is_match(santizied_temporal_value)
        {
            trace!("Parsing a date (ordinal or not)");
            Date::from_str(santizied_temporal_value).map(|date| date.into())
        } else if RE_TIME_HMS_OFFSET_MERIDAN.is_match(santizied_temporal_value) {
            trace!("Parsing a time + offset + merdian value.");
            Time::from_str(santizied_temporal_value).map(|time| time.into())
        } else if values.len() != 1 {
            let mut date = None;
            let mut time = None;
            let mut offset = None;

            for value in values
                .iter()
                .cloned()
                .map(|v| temporal::Stamp::parse(&v))
                .flatten()
            {
                if value.is_date() && date.is_none() {
                    date = value.as_date()
                } else if value.is_time() && time.is_none() {
                    time = value.as_time();
                } else if value.is_offset() && offset.is_none() {
                    offset = value.as_offset();
                }
            }

            time = if let Some(mut t) = time {
                if t.prefix.is_none() {
                    t.prefix = Some(' ');
                }
                Some(t)
            } else {
                time
            };

            temporal::Stamp::compose(date, time, offset)
                .ok_or_else(|| Error::CompletelyInvalid(temporal_value.to_string()))
        } else {
            trace!("{:?} didn't match anything for representing a timestamp or things related to it (time, date, offset).", temporal_value);
            Err(Error::CompletelyInvalid(temporal_value.to_string()))
        }
    }

    pub fn is_date(&self) -> bool {
        self.date.is_some() && self.time.is_none()
    }
    pub fn is_time(&self) -> bool {
        self.date.is_none() && self.time.is_some()
    }
    pub fn is_offset(&self) -> bool {
        self.date.is_none() && self.time.is_none() && self.offset.is_some()
    }

    pub fn as_date(&self) -> Option<Date> {
        self.date.clone()
    }
    pub fn as_time(&self) -> Option<Time> {
        self.time.clone()
    }
    pub fn as_offset(&self) -> Option<Offset> {
        self.offset.clone()
    }

    /// Merges a date, time and offset conditionally into a singluar datetime with offset.
    pub fn compose(date: Option<Date>, time: Option<Time>, offset: Option<Offset>) -> Option<Self> {
        if date.is_none() && time.is_none() && offset.is_none() {
            None
        } else {
            Some(Self {
                date,
                time,
                offset,
                was_iso8601: false,
            })
        }
    }
}

impl FromStr for Stamp {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::parse(s)
    }
}

impl From<Date> for Stamp {
    fn from(value: Date) -> Self {
        Self {
            date: Some(value),
            ..Default::default()
        }
    }
}

impl From<Time> for Stamp {
    fn from(value: Time) -> Self {
        Self {
            time: Some(value),
            ..Default::default()
        }
    }
}

impl From<Offset> for Stamp {
    fn from(value: Offset) -> Self {
        Self {
            offset: Some(value),
            ..Default::default()
        }
    }
}

impl From<chrono::DateTime<chrono::Utc>> for Stamp {
    fn from(dt: chrono::DateTime<chrono::Utc>) -> Self {
        Self {
            time: Some(Time::from_time(dt.time(), true, Some('T'), None)),
            date: Some(Date::from_date(dt.date().naive_utc(), false, true)),
            offset: Some(Offset::from_offset(chrono::Offset::fix(dt.offset()))),
            was_iso8601: true,
        }
    }
}

impl From<chrono::DateTime<chrono::FixedOffset>> for Stamp {
    fn from(dt: chrono::DateTime<chrono::FixedOffset>) -> Self {
        Self {
            time: Some(Time::from_time(dt.time(), true, Some('T'), None)),
            date: Some(Date::from_date(dt.date().naive_local(), false, true)),
            offset: Some(Offset::from_offset(*dt.offset())),
            was_iso8601: true,
        }
    }
}

impl serde::Serialize for Stamp {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let stamp_string = self.to_string();
        stamp_string.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Stamp {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let stamp_string = String::deserialize(deserializer)?;
        Self::from_str(&stamp_string)
            .map_err(serde::de::Error::custom)
            .and_then(|t| {
                if stamp_string != t.to_string() {
                    Err(serde::de::Error::invalid_type(
                        de::Unexpected::Str("arbitrary string"),
                        &"temporal value being a timestamp, date, time or an offset",
                    ))
                } else {
                    Ok(t)
                }
            })
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Duration {
    pub year: Option<u32>,
    pub month: Option<u32>,
    pub week: Option<u32>,
    pub day: Option<u32>,
    pub time: Option<Time>,
}

impl FromStr for Duration {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(parts) = RE_DURATION.captures(s) {
            trace!("This matched our regex for durations.");
            let year = parts.name("year").and_then(|v| v.as_str().parse().ok());
            let month = parts.name("month").and_then(|v| v.as_str().parse().ok());
            let week = parts.name("week").and_then(|v| v.as_str().parse().ok());
            let day = parts.name("day").and_then(|v| v.as_str().parse().ok());

            let time = if parts.name("time").is_some() {
                let hour = parts
                    .name("hour")
                    .and_then(|v| v.as_str().parse().ok())
                    .unwrap_or(0);
                let minute = parts
                    .name("minute")
                    .and_then(|v| v.as_str().parse().ok())
                    .unwrap_or(0);
                let second = parts
                    .name("second")
                    .and_then(|v| v.as_str().parse().ok())
                    .unwrap_or(0);

                Some(Time::from_str(&format!("{}:{}:{}", hour, minute, second))?)
            } else {
                None
            };

            Ok(Self {
                year,
                month,
                week,
                day,
                time,
            })
        } else {
            trace!("{} didn't match the regex for durations.", s);
            Err(Error::CompletelyInvalid(s.to_string()))
        }
    }
}

impl ToString for Duration {
    fn to_string(&self) -> String {
        let mut period = vec![];
        let mut time = vec![];

        if let Some(y) = self.year {
            period.push(format!("{}Y", y));
        }

        if let Some(m) = self.month {
            period.push(format!("{}M", m));
        }

        if let Some(w) = self.week {
            period.push(format!("{}W", w));
        }

        if let Some(d) = self.day {
            period.push(format!("{}D", d));
        }

        if let Some(t) = &self.time {
            time.push(format!("{}H", t.data.hour()));
            time.push(format!("{}M", t.data.minute()));
            time.push(format!("{}S", t.data.second()));
        }

        format!("P{}{}", period.join(""), time.join(""))
    }
}

// #[derive(Debug, Clone)]
// pub enum Interval {
//     Within(Stamp, Stamp),
//     After(Stamp, Duration),
//     Until(Duration, Stamp),
//     Periodic(Duration),
// }

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Value {
    Duration(Duration),
    Timestamp(Stamp),
}

impl ToString for Value {
    fn to_string(&self) -> String {
        match self {
            Self::Duration(d) => d.to_string(),
            Self::Timestamp(t) => t.to_string(),
        }
    }
}

impl FromStr for Value {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Duration::from_str(s)
            .map(Self::Duration)
            .or_else(|_| Stamp::from_str(s).map(Self::Timestamp))
    }
}

impl serde::Serialize for Value {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let value_string = self.to_string();
        value_string.serialize(serializer)
    }
}

impl<'de> serde::Deserialize<'de> for Value {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value_string = String::deserialize(deserializer)?;
        Self::from_str(&value_string).map_err(serde::de::Error::custom)
    }
}

#[test]
fn value_to_string() {
    crate::test::enable_logging();
    assert_eq!(
        Value::from_str("P2Y").map(|s| s.to_string()),
        Ok("P2Y".to_string())
    );
}

#[test]
fn stamp_to_string() {
    crate::test::enable_logging();
    assert_eq!(
        Stamp::from_str("2000-10-01 1:00").map(|s| s.to_string()),
        Ok("2000-10-01 01:00".to_owned())
    );
    assert_eq!(
        Stamp::from_str("19:00:00-08:00").map(|s| s.to_string()),
        Ok("19:00:00-0800".to_owned())
    );
    assert_eq!(
        Stamp::from_str("2000-10-01 19:00:00-08:00").map(|s| s.to_string()),
        Ok("2000-10-01 19:00:00-0800".to_owned())
    );
    assert_eq!(
        Stamp::from_str("2000-10-01 19:00:00-0800").map(|s| s.to_string()),
        Ok("2000-10-01 19:00:00-0800".to_owned())
    );
    assert_eq!(
        Stamp::from_str("2009-06-26T19:00:00-08:00").map(|s| s.to_string()),
        Ok("2009-06-26T19:00:00-0800".to_owned())
    );
    assert_eq!(
        Stamp::from_str("2009-06-26T19:00-08:00").map(|s| s.to_string()),
        Ok("2009-06-26T19:00-0800".to_owned()),
        "keeping the colon out of the output"
    );
    assert_eq!(
        Stamp::from_str("2009-06-26T19:00-08").map(|s| s.to_string()),
        Ok("2009-06-26T19:00-08".to_owned()),
        "don't add the minutes in the offset"
    );
    assert_eq!(
        Stamp::from_str("2009-06-26T19:00-0800").map(|s| s.to_string()),
        Ok("2009-06-26T19:00-0800".to_owned()),
        "no colon in offset, looking ISO8601-y"
    );
    assert_eq!(
        Stamp::from_str("2009-06-26 19:00+0800").map(|s| s.to_string()),
        Ok("2009-06-26 19:00+0800".to_owned()),
        "maintain lack of colon in non-ISO8601 value"
    );
}
