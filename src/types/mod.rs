use chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime};
use log::*;
use regex::Regex;
use serde::{
    de::{self, MapAccess, Visitor},
    ser::{SerializeMap, SerializeSeq, SerializeStruct},
    Deserialize, Deserializer, Serializer,
};
use std::{
    cell::RefCell,
    cmp::Ordering,
    collections::HashMap,
    convert::{Infallible, TryFrom, TryInto},
    fmt::{self, Debug},
    ops::Deref,
    rc::Rc,
    str::FromStr,
};
pub use url::Url;
pub mod temporal;

#[cfg(test)]
mod test;

pub type NodeList = Vec<PropertyValue>;
pub type Properties = HashMap<String, NodeList>;

/// A concrete reference of the supported Microformats class by this library.
#[derive(serde::Serialize, serde::Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
#[serde(rename_all = "kebab-case")]
pub enum KnownClass {
    /// Represents a general container for content.
    /// https://microformats.org/wiki/h-entry
    #[serde(alias = "h-entry")]
    Entry,

    /// Represents a rewference to content at a different location.
    /// https://microformats.org/wiki/h-cite
    #[serde(alias = "h-cite")]
    Cite,

    /// Represents a contact card or vCard.
    /// https://microformats.org/wiki/h-card
    #[serde(alias = "h-card")]
    Card,

    /// https://microformats.org/wiki/h-feed
    #[serde(alias = "h-feed")]
    Feed,

    /// https://microformats.org/wiki/h-event
    #[serde(alias = "h-event")]
    Event,

    /// https://microformats.org/wiki/h-product
    #[serde(alias = "h-product")]
    Product,

    /// https://microformats.org/wiki/h-adr
    #[serde(alias = "h-adr")]
    Adr,

    /// https://microformats.org/wiki/h-geo
    #[serde(alias = "h-geo")]
    Geo,

    /// https://microformats.org/wiki/h-resume
    #[serde(alias = "h-resume")]
    Resume,

    /// https://microformats.org/wiki/h-review
    #[serde(alias = "h-review")]
    Review,

    /// https://microformats.org/wiki/h-recipe
    #[serde(alias = "h-recipe")]
    Recipe,
}

impl FromStr for KnownClass {
    type Err = super::Error;

    /// Converts this concrete known class into a string.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_ascii_lowercase().as_str() {
            "h-entry" | "entry" => Ok(Self::Entry),
            "h-cite" | "cite" => Ok(Self::Cite),
            "h-card" | "card" => Ok(Self::Card),
            "h-event" | "event" => Ok(Self::Event),
            "h-product" | "product" => Ok(Self::Product),
            "h-feed" | "feed" => Ok(Self::Feed),
            "h-geo" | "geo" => Ok(Self::Geo),
            "h-adr" | "adr" => Ok(Self::Adr),
            "h-recipe" | "recipe" => Ok(Self::Recipe),
            _ => Err(super::Error::NotKnownClass(s.to_string())),
        }
    }
}

impl ToString for KnownClass {
    /// Converts a known class into a string.
    fn to_string(&self) -> String {
        match self {
            KnownClass::Entry => "h-entry".to_owned(),
            KnownClass::Cite => "h-cite".to_owned(),
            KnownClass::Card => "h-card".to_owned(),
            KnownClass::Feed => "h-feed".to_owned(),
            KnownClass::Event => "h-event".to_owned(),
            KnownClass::Product => "h-product".to_owned(),
            KnownClass::Adr => "h-adr".to_owned(),
            KnownClass::Geo => "h-geo".to_owned(),
            KnownClass::Resume => "h-resume".to_owned(),
            KnownClass::Review => "h-review".to_owned(),
            KnownClass::Recipe => "h-recipe".to_owned(),
        }
    }
}

/// Represents a Microformat class.
/// https://microformats.org/wiki/Category:Draft_Specifications
#[derive(Debug, Clone, Eq)]
pub enum Class {
    /// Represents a known Microformat class (h-entry, h-card, etc).
    Known(KnownClass),

    /// Represents a class that's not spec-compliant (h-cookies, h-monster, etc).
    Unrecognized(String),

    /// Represents a custom vendor specific or experimental Microformat class (h-x-toy)
    // FIXME: Correct this to expect two strings (vendor prefix and type)
    VendorSpecific(String),
}

impl PartialOrd for Class {
    fn partial_cmp(&self, other: &Self) -> std::option::Option<std::cmp::Ordering> {
        self.to_string().partial_cmp(&other.to_string())
    }
}

impl PartialEq for Class {
    fn eq(&self, other: &Self) -> bool {
        self.to_string().eq(&other.to_string())
    }
}

impl FromStr for Class {
    type Err = Infallible;

    /// Parses a string as a Microformat class.
    ///
    /// # Examples
    /// ```
    /// # use std::str::FromStr;
    /// # use microformats::types::{Class, KnownClass};
    ///
    /// assert_eq!(Class::from_str("entry"), Ok(Class::Known(KnownClass::Entry)));
    /// assert_eq!(Class::from_str("h-card"), Ok(Class::Known(KnownClass::Card)));
    /// assert_eq!(Class::from_str("h-x-plane"), Ok(Class::VendorSpecific("plane".to_string())));
    /// ```
    fn from_str(class_str: &str) -> Result<Self, Self::Err> {
        if class_str.starts_with("h-x-") {
            Ok(Self::VendorSpecific(
                class_str.get(4..).unwrap_or_default().to_string(),
            ))
        } else {
            match KnownClass::from_str(class_str)
                .or_else(|_| KnownClass::from_str(&class_str.replace("h-", "")))
            {
                Ok(known_class) => Ok(Self::Known(known_class)),
                Err(_) => Ok(Self::Unrecognized(
                    class_str.trim_start_matches("h-").to_string(),
                )),
            }
        }
    }
}

impl ToString for Class {
    /// Converts a Microformat class representation to its string form.
    fn to_string(&self) -> String {
        match self {
            Self::Known(class) => class.to_string(),
            Self::Unrecognized(class) => format!("h-{}", class),
            Self::VendorSpecific(class) => format!("h-x-{}", class),
        }
    }
}

impl serde::Serialize for Class {
    /// Serializes this class reference into a string.
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}

impl Class {
    /// Checks if this class is a recognized one by the Microformats spec.
    pub fn is_recognized(&self) -> bool {
        !matches!(self, Self::Unrecognized(_))
    }
}

struct ClassVisitor;

impl<'de> Visitor<'de> for ClassVisitor {
    type Value = Class;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a string that follows Microformats class conventions")
    }

    fn visit_str<E>(self, class_str: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Class::from_str(class_str).map_err(|e| E::custom(e.to_string()))
    }
}

impl<'de> serde::Deserialize<'de> for Class {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_string(ClassVisitor)
    }
}

fn short_circuit_url_deserialization<'de, D>(d: D) -> Result<Url, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let string_form = String::deserialize(d)?;
    let url_form = Url::parse(&string_form).map_err(serde::de::Error::custom)?;

    if url_form.as_str() != string_form {
        // This is called in the event a string happens to match the parsing of a URL but doesn't
        // convert back into one.
        Err(serde::de::Error::custom(
            "This string doesn't represent a valid URL due looking like one.",
        ))
    } else {
        Ok(url_form)
    }
}

fn short_circuit_plain_text_deserialization<'de, D>(d: D) -> Result<String, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let string_form = String::deserialize(d)?;
    trace!(
        "Checking if {:?} can be represented as a different value.",
        string_form
    );

    Url::from_str(&string_form)
        .map_err(serde::de::Error::custom)
        .map(|u| u.as_str().to_string())
        .and_then(|u| {
            if u == string_form && !u.contains(|c: char| c.is_whitespace()) && !u.contains('\n') {
                trace!("{:?} is actually a URL", u);
                Err(serde::de::Error::invalid_type(
                    de::Unexpected::Other("URL"),
                    &"plain 'ol string",
                ))
            } else {
                Ok(string_form.clone())
            }
        })
        .or_else(|r: D::Error| {
            if r.to_string().starts_with("invalid type: URL") {
                Err(r)
            } else {
                temporal::Value::from_str(&string_form)
                    .map_err(serde::de::Error::custom)
                    .map(|u| u.to_string())
                    .and_then(|u| {
                        if u == string_form {
                            trace!("{:?} is actually a temporal value", string_form);
                            Err(serde::de::Error::invalid_type(
                                de::Unexpected::Str("temporal data"),
                                &"plain 'ol string",
                            ))
                        } else {
                            Ok(string_form.clone())
                        }
                    })
            }
        })
        .or_else(|r: D::Error| {
            if r.to_string().starts_with("invalid type: URL")
                || r.to_string().contains("temporal data")
            {
                Err(r)
            } else {
                trace!("Looks like {:?} is just a string.", string_form);
                Ok(string_form)
            }
        })
}

/// Represents the multiple forms in which a property's value is represented.
#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, PartialOrd, Ord)]
#[serde(untagged, rename_all = "kebab-case")]
pub enum PropertyValue {
    /// Represents plain-text, usually stored in properties like "p-name". It can be
    /// displayed directly to the user agent.
    /// <https://microformats.org/wiki/microformats2-parsing#parsing_a_p-_property>
    #[serde(deserialize_with = "short_circuit_plain_text_deserialization")]
    Plain(String),

    /// Represents a linked value.
    /// It's a companion of `Plain`; meant to make resolving non-absolute URLs easier.
    #[serde(deserialize_with = "short_circuit_url_deserialization")]
    Url(Url),

    /// Represents a compatible datetime parser defined by <https://microformats.org/wiki/value-class-pattern#Date_and_time_parsing>
    /// <https://microformats.org/wiki/microformats2-parsing#parsing_a_dt-_property>
    Temporal(temporal::Value),

    Fragment(Fragment),

    Image(Image),

    /// Represents a structured form of information presented by Microformats as an
    /// `Item`. This will usually require a bit more processing before showing it.
    /// <https://microformats.org/wiki/microformats2-parsing#parsing_a_u-_property>
    #[serde(with = "referenced_item")]
    Item(Rc<RefCell<Item>>),
}

/// Represents markup and the plain text representation accompanying it.
/// <https://microformats.org/wiki/microformats2-parsing#parsing_a_e-_property>
#[derive(
    Debug, Clone, PartialEq, Eq, serde::Serialize, Default, serde::Deserialize, PartialOrd, Ord,
)]
#[serde(rename_all = "kebab-case")]
pub struct Fragment {
    /// Provides the HTML representation of this fragment.
    #[serde(skip_serializing_if = "String::is_empty")]
    pub html: String,

    /// Provides the plain-text form of the HTML.
    #[serde(default, skip_serializing_if = "String::is_empty")]
    pub value: String,

    /// Provides the language that this fragment is represented in.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub lang: Option<String>,
}

impl Fragment {
    /// Determines if this fragment has any information represented in it.
    ///
    /// The presence of HTML does not determine if this fragment is empty; a
    /// fragment can be defined with no HTML (meaning the HTML would implictly look the same).
    pub fn is_empty(&self) -> bool {
        self.value.is_empty()
    }
}

/// Represents the structured form of an image.
/// <https://microformats.org/wiki/microformats2-parsing#parse_an_img_element_for_src_and_alt>
#[derive(Debug, Clone, PartialEq, Eq, serde::Deserialize, serde::Serialize, PartialOrd, Ord)]
#[serde(rename_all = "kebab-case")]
pub struct Image {
    #[serde(alias = "value", alias = "src")]
    pub src: Url,

    #[serde(skip_serializing_if = "String::is_empty")]
    pub alt: String,
}

mod referenced_item {

    use super::*;

    type Value = Rc<RefCell<Item>>;

    struct ItemVisitor;

    #[derive(serde::Deserialize, Debug)]
    #[serde(field_identifier, rename_all = "kebab-case")]
    enum ItemDeserializationFields {
        Children,
        Value,
        Id,
        Properties,
        r#Type,
    }

    impl<'de> Visitor<'de> for ItemVisitor {
        type Value = Value;
        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("expecting null or an map representing an item")
        }

        fn visit_map<A>(self, mut item_map: A) -> Result<Self::Value, A::Error>
        where
            A: MapAccess<'de>,
        {
            let mut children: Vec<Item> = Default::default();
            let mut value = None;
            let mut id = None;
            let mut types = Vec::new();
            let mut properties = Properties::new();

            while let Some(property) = item_map.next_key()? {
                match property {
                    ItemDeserializationFields::Children => {
                        trace!("Adding in children");
                        let new_items = item_map.next_value::<Vec<Item>>()?.into_iter();

                        if children.is_empty() && new_items.len() > 0 {
                            children = new_items.collect();
                        } else {
                            children.extend(new_items);
                        }
                    }
                    ItemDeserializationFields::Value => {
                        trace!("Adding in value");
                        if value.is_none() {
                            value = item_map.next_value::<Option<ValueKind>>()?;
                        }
                    }
                    ItemDeserializationFields::Id => {
                        trace!("Adding in ID");
                        if id.is_none() {
                            id = item_map.next_value()?;
                        }
                    }
                    ItemDeserializationFields::Type => {
                        trace!("Adding in types");
                        types.extend(item_map.next_value::<Vec<Class>>()?);
                    }
                    ItemDeserializationFields::Properties => {
                        trace!("Add in properties.");
                        properties.extend(item_map.next_value::<Properties>()?);
                    }
                }
            }

            let mapped_children = children
                .into_iter()
                .map(|i| Rc::new(RefCell::new(i)))
                .collect::<Vec<_>>();

            Ok(Rc::new(RefCell::new(Item {
                parent: Rc::new(ParentRelationship::Root),
                r#type: types,
                properties: Rc::new(RefCell::new(properties)),
                id,
                value,
                children: mapped_children,
            })))
        }
    }

    pub fn serialize<S>(item: &Value, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        if let Ok(concrete_item) = item.deref().try_borrow() {
            serializer.serialize_some(&Some(concrete_item.deref()))
        } else {
            serializer.serialize_none()
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        trace!("Attempting to deserialize a referenced item.");
        deserializer.deserialize_struct(
            "Item",
            &["type", "properties", "id", "value", "children"],
            ItemVisitor,
        )
    }
}

impl PropertyValue {
    /// Determines if this node's internal value is empty
    pub fn is_empty(&self) -> bool {
        match self {
            Self::Temporal(_) | Self::Url(_) => false,
            Self::Image(_) => false,
            Self::Plain(s) => s.is_empty(),
            Self::Fragment(f) => f.is_empty(),
            Self::Item(i) => i.borrow().is_empty(),
        }
    }
}

impl From<Url> for PropertyValue {
    fn from(u: Url) -> Self {
        Self::Url(u)
    }
}

impl From<Rc<RefCell<Item>>> for PropertyValue {
    fn from(item: Rc<RefCell<Item>>) -> Self {
        Self::Item(Rc::clone(&item))
    }
}

impl From<temporal::Stamp> for PropertyValue {
    fn from(t: temporal::Stamp) -> Self {
        Self::Temporal(temporal::Value::Timestamp(t))
    }
}

impl From<temporal::Duration> for PropertyValue {
    fn from(t: temporal::Duration) -> Self {
        Self::Temporal(temporal::Value::Duration(t))
    }
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum RelationshipKind {
    /// p-
    Plain(String),
    /// u-
    URL(String),
    /// dt-
    Datetime(String),
    /// e-
    HTML(String),
}

/// Represents the relationship an item has to its parent (if any).
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum ParentRelationship {
    /// Another item is the direct parent of this item (either via `Item::children` or as a direct
    /// item in a list of values in `Item::properties`).
    Child(Rc<RefCell<Item>>),

    /// Another item is the direct parent of this item by way of a property type.
    Property(RelationshipKind, Rc<RefCell<Item>>),

    /// A top-level item; the document itself is the parent.
    // FIXME: Should this have a reference to the actual document?
    Root,
}

impl Debug for ParentRelationship {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Root => f.write_str("Root"),
            Self::Child(item) => match item.try_borrow() {
                Ok(borrowed_item) => f.write_str(&format!(
                    "ChildOfItem({:?})",
                    borrowed_item
                        .id
                        .clone()
                        .or_else(|| Some(format!("{:?}", borrowed_item.value)))
                )),
                Err(_) => f.write_str("ChildOfItem(<borrowed>)"),
            },
            Self::Property(kind, item) => match item.try_borrow() {
                Ok(borrowed_item) => f.write_str(&format!(
                    "PropertyOf({:?}) as {:?}",
                    borrowed_item
                        .id
                        .clone()
                        .or_else(|| Some(format!("{:?}", borrowed_item.value))),
                    kind
                )),
                Err(_) => f.write_str(&format!("PropertyOf(<borrowed>) as {:?}", kind)),
            },
        }
    }
}

impl ParentRelationship {
    /// Creates a new child element for this item parent.
    pub fn create_child_item(
        parent: Rc<Self>,
        document: Rc<RefCell<Document>>,
        types: &[Class],
    ) -> Rc<RefCell<Item>> {
        match parent.deref() {
            Self::Root => Rc::clone(&document.deref().borrow_mut().create_child_item(types)),
            Self::Child(parent_item) | Self::Property(_, parent_item) => {
                let new_item = Rc::new(Item::new(Rc::clone(&parent), types.to_vec()));
                parent_item.borrow_mut().append(&new_item);
                Rc::clone(&new_item)
            }
        }
    }

    /// Creates a new top-level item or resolves the current one.
    pub fn resolve_item(&self) -> Option<Rc<RefCell<Item>>> {
        match self {
            Self::Root => None,
            Self::Child(current_item) => Some(Rc::clone(current_item)),
            Self::Property(_, current_item) => Some(Rc::clone(current_item)),
        }
    }
}

impl Default for ParentRelationship {
    /// Defines a default value to be to `ItemParent::Document`.
    fn default() -> Self {
        ParentRelationship::Root
    }
}

/// Represents the structured form of an 'object' in Microformats.
// FIXME: Move to use 'std::rc::Weak' for the parent reference.
#[derive(serde::Serialize, serde::Deserialize, Clone, Default, Debug, PartialEq, Eq)]
#[serde(rename_all = "kebab-case")]
pub struct Item {
    pub r#type: Vec<Class>,

    /// Represents the directly associated attributes for this item.
    #[serde(with = "referenced_properties")]
    pub properties: Rc<RefCell<Properties>>,

    /// Represents a list of children for this item.
    #[serde(
        default,
        with = "referenced_children",
        skip_serializing_if = "Vec::is_empty"
    )]
    pub children: Vec<Rc<RefCell<Item>>>,

    /// The ID string of this item, if any is resolved.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// Represents the precise value of this item (if it's defined as a property to another).
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub value: Option<ValueKind>,

    #[serde(skip)]
    parent: Rc<ParentRelationship>,
}

impl PartialOrd for Item {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self.id.partial_cmp(&other.id) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        match self.value.partial_cmp(&other.value) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }

        self.parent.partial_cmp(&other.parent)
    }
}

impl Ord for Item {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let id = self.id.cmp(&other.id);
        let value = self.value.cmp(&other.value);

        if id != Ordering::Equal {
            id
        } else if value != Ordering::Equal {
            value
        } else {
            Ordering::Equal
        }
    }
}

impl TryFrom<serde_json::Map<String, serde_json::Value>> for Item {
    type Error = crate::Error;

    fn try_from(obj: serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        if !obj.contains_key("type") {
            return Err(Self::Error::ObjectMissingProperty("type".to_string()));
        }
        if !obj.contains_key("properties") {
            return Err(Self::Error::ObjectMissingProperty("properties".to_string()));
        }

        serde_json::from_value(serde_json::Value::Object(obj)).map_err(Self::Error::JSON)
    }
}

impl TryFrom<serde_json::Value> for Item {
    type Error = crate::Error;

    fn try_from(v: serde_json::Value) -> Result<Self, Self::Error> {
        if let serde_json::Value::Object(o) = v {
            Self::try_from(o)
        } else {
            Err(Self::Error::NotAnObject)
        }
    }
}

impl TryInto<serde_json::Value> for Item {
    type Error = crate::Error;

    fn try_into(self) -> Result<serde_json::Value, Self::Error> {
        serde_json::to_value(self).map_err(crate::Error::JSON)
    }
}

impl IntoIterator for Item {
    type Item = Item;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        if self.children.is_empty() {
            vec![self].into_iter()
        } else {
            // FIXME: Add self to this list.
            let mut items = vec![];
            items.push(self.clone());
            items.extend(
                self.children
                    .into_iter()
                    .flat_map(|item| item.borrow().clone().into_iter())
                    .collect::<Vec<_>>(),
            );
            items.into_iter()
        }
    }
}

#[derive(serde::Serialize, serde::Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
#[serde(untagged, rename_all = "kebab-case")]
pub enum ValueKind {
    Url(Url),
    Plain(String),
}

impl Default for ValueKind {
    fn default() -> Self {
        Self::Plain(String::default())
    }
}

mod referenced_children {

    use super::*;
    type Value = Vec<Rc<RefCell<Item>>>;

    struct ChildrenVisitor;

    impl<'de> Visitor<'de> for ChildrenVisitor {
        type Value = Value;
        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("expecting a list of children nodes, an empty list or null")
        }

        fn visit_seq<ChildrenSequenceAccessor>(
            self,
            mut seq: ChildrenSequenceAccessor,
        ) -> Result<Self::Value, ChildrenSequenceAccessor::Error>
        where
            ChildrenSequenceAccessor: de::SeqAccess<'de>,
        {
            let size_hint = seq.size_hint().unwrap_or(0);
            let mut children = Vec::with_capacity(size_hint);

            while let Some(item) = seq.next_element()? {
                log::trace!("converting to item {:#?}", item);
                children.push(Rc::new(RefCell::new(item)));
            }

            Ok(children)
        }
    }

    #[allow(clippy::ptr_arg)]
    pub fn serialize<S>(children: &Value, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(children.len()))?;
        let safe_items = children
            .iter()
            .filter_map(|item| item.try_borrow().ok())
            .filter(|item| !item.is_empty())
            .map(|item| item.deref().clone())
            .collect::<Vec<_>>();
        for concrete_item in safe_items {
            seq.serialize_element(&concrete_item)?;
        }
        seq.end()
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Value, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_seq(ChildrenVisitor)
    }
}

mod referenced_properties {
    use super::*;
    type Value = Rc<RefCell<Properties>>;

    struct PropertyVisitor;

    #[derive(serde::Deserialize, Debug)]
    #[serde(untagged)]
    enum PotentialValues {
        List(NodeList),
        Value(PropertyValue),
    }

    impl<'de> Visitor<'de> for PropertyVisitor {
        type Value = Value;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a map of properties with values that could be null, a string, a list of either strings, maps or both")
        }

        fn visit_map<A>(self, mut map_visitor: A) -> Result<Self::Value, A::Error>
        where
            A: de::MapAccess<'de>,
        {
            let size_hint = map_visitor.size_hint().unwrap_or(0);
            let mut property_map = Properties::with_capacity(size_hint);

            while let Some(key) = map_visitor.next_key()? {
                let concrete_value: NodeList = match map_visitor.next_value::<PotentialValues>()? {
                    PotentialValues::List(values) => values,
                    PotentialValues::Value(node) => vec![node],
                };

                // FIXME: Use `Properties:try_insert` to prevent overwriting of values; merge them.
                property_map.insert(key, concrete_value);
            }

            Ok(Value::new(RefCell::new(property_map)))
        }
    }

    pub fn serialize<S>(properties: &Value, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        let concrete_properties: &Properties = &*properties.deref().borrow();
        let mut properties_seq = serializer.serialize_map(Some(concrete_properties.len()))?;

        for (key, value) in concrete_properties {
            properties_seq.serialize_entry(key, value)?;
        }

        properties_seq.end()
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Value, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_map(PropertyVisitor)
    }
}

impl Item {
    /// Provides the parent item of this item.
    pub fn parent(&self) -> Rc<ParentRelationship> {
        Rc::clone(&self.parent)
    }

    /// Returns this Item without a reference to the document.
    pub fn dangle(&self) -> Self {
        let mut dangling_item = self.clone();
        dangling_item.parent = Rc::new(ParentRelationship::Root);
        dangling_item
    }

    /// Creates a new item with the provided `ItemParent` as its parent.
    pub fn new(parent: Rc<ParentRelationship>, types: Vec<Class>) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Item {
            r#type: types,
            parent: Rc::clone(&parent),
            children: Vec::default(),
            properties: Rc::default(),
            value: None,
            id: None,
        }))
    }

    /// Adds a Item to this Item as a child.
    pub fn append(&mut self, item: &Rc<RefCell<Self>>) {
        self.children.push(Rc::clone(item));
    }

    /// Determines if this item is undefined - an empty one.
    pub fn is_empty(&self) -> bool {
        self.children.is_empty() && self.r#type.is_empty()
    }

    pub fn remove_child(&mut self, problem_child: Rc<RefCell<Self>>) {
        if let Some(position) = self.children.iter().position(|v| v == &problem_child) {
            trace!(
                "Attempting to remove the child {:#?} from {:#?}",
                problem_child,
                self.children
            );
            self.children.remove(position);
        }
    }

    pub fn remove_whole_property(&mut self, property_name: &str) {
        let mut props = self.properties.borrow_mut();
        trace!(
            "Removed {:?} from this item's properties; which contained {:#?}",
            property_name,
            props.remove(property_name)
        );
    }

    pub fn content(&self) -> Option<Vec<PropertyValue>> {
        self.properties.borrow().deref().get("content").cloned()
    }

    pub fn append_property(&mut self, property_name: &str, property_value: PropertyValue) {
        let mut props = self.properties.borrow_mut();
        let mut new_values = if let Some(values) = props.get(property_name) {
            values.to_vec()
        } else {
            Vec::default()
        };

        trace!(
            "Appending the value {:?} to the {:?} property",
            property_value,
            property_name
        );
        new_values.push(property_value);

        props.insert(property_name.to_owned(), new_values);
    }

    /// Checks if this item has any properties with a nested item.
    pub fn has_nested_microformats(&self) -> bool {
        let props = self.properties.borrow();
        let has_nested_value_microformats = props
            .values()
            .flatten()
            .any(|v| matches!(v, PropertyValue::Item(_)));

        !has_nested_value_microformats && !self.children.is_empty()
    }

    pub fn nested_children(&self) -> Vec<Rc<RefCell<Item>>> {
        self.properties
            .borrow()
            .values()
            .map(|v| v.iter())
            .flatten()
            .filter_map(|value| {
                if let PropertyValue::Item(item) = value {
                    Some(Rc::clone(item))
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
    }

    pub fn has_id(item: Rc<RefCell<Self>>, id: &str) -> Option<Rc<RefCell<Item>>> {
        item.borrow()
            .id
            .as_ref()
            .and_then(|my_id| {
                if my_id == id {
                    Some(Rc::clone(&item))
                } else {
                    None
                }
            })
            .or_else(|| {
                item.borrow()
                    .children
                    .iter()
                    .find_map(|item| Self::has_id(Rc::clone(item), id))
            })
            .or_else(|| {
                item.borrow()
                    .nested_children()
                    .iter()
                    .find_map(|item| Self::has_id(Rc::clone(item), id))
            })
    }

    pub fn has_url(item: Rc<RefCell<Self>>, url: &Url) -> Option<PropertyValue> {
        item.borrow()
            .properties
            .borrow()
            .get("url")
            .and_then(|urls| {
                if urls.contains(&PropertyValue::Url(url.to_owned())) {
                    Some(PropertyValue::Item(Rc::clone(&item)))
                } else {
                    None
                }
            })
            .or_else(|| {
                item.borrow()
                    .children
                    .iter()
                    .find_map(|item| Self::has_url(Rc::clone(item), url))
            })
            .or_else(|| {
                item.borrow()
                    .nested_children()
                    .iter()
                    .find_map(|item| Self::has_url(Rc::clone(item), url))
            })
    }

    pub fn set_parent(&mut self, property_relationship: Rc<ParentRelationship>) {
        self.parent = property_relationship;
    }

    pub fn properties_with_matching_value(&self, property_value: PropertyValue) -> Vec<String> {
        let self_properties: Vec<_> = self
            .properties
            .borrow()
            .iter()
            .filter_map(|(property_name, property_values)| {
                property_values.iter().find_map(|value| {
                    let pv = property_value.clone();
                    if *value == pv {
                        return Some(property_name.to_owned());
                    }

                    debug!("Comparing {:?} to {:?}", pv, value);

                    match value {
                        PropertyValue::Plain(p) => {
                            if let PropertyValue::Url(u) = &pv {
                                if p.contains(&u.to_string()) {
                                    Some(property_name.to_owned())
                                } else {
                                    None
                                }
                            } else {
                                None
                            }
                        }
                        PropertyValue::Url(u) => {
                            if pv == PropertyValue::Plain(u.to_string()) {
                                Some(property_name.to_owned())
                            } else {
                                None
                            }
                        }
                        PropertyValue::Temporal(_) => None,
                        PropertyValue::Fragment(Fragment { html, value, .. }) => {
                            if let PropertyValue::Url(u) = &pv {
                                if html.contains(&u.to_string()) || value.contains(&u.to_string()) {
                                    Some(property_name.to_owned())
                                } else {
                                    None
                                }
                            } else if let PropertyValue::Plain(p) = &pv {
                                if html.contains(p) || value.contains(p) {
                                    Some(property_name.to_owned())
                                } else {
                                    None
                                }
                            } else {
                                None
                            }
                        }
                        PropertyValue::Image(Image { src, alt }) => {
                            if let PropertyValue::Url(u) = &pv {
                                if src == u {
                                    Some(property_name.to_owned())
                                } else {
                                    None
                                }
                            } else if let PropertyValue::Plain(t) = &pv {
                                if alt == t {
                                    Some(property_name.to_owned())
                                } else {
                                    None
                                }
                            } else {
                                None
                            }
                        }
                        PropertyValue::Item(item) => {
                            if let PropertyValue::Url(u) = &pv {
                                let props = item.borrow().properties.borrow().clone();

                                // FIXME: Check fragment of URL if self-page.
                                let urns = [
                                    props.get("url").cloned().unwrap_or_default(),
                                    props.get("uid").cloned().unwrap_or_default(),
                                ]
                                .iter()
                                .flatten()
                                .cloned()
                                .collect::<Vec<_>>();

                                if let Some(v) = &item.borrow().value {
                                    match v {
                                        ValueKind::Url(vu) if vu == u => {
                                            Some(property_name.to_owned())
                                        }
                                        ValueKind::Plain(vp) if *vp == u.to_string() => {
                                            Some(property_name.to_owned())
                                        }
                                        _ => None,
                                    }
                                } else if urns.iter().any(|v| *v == PropertyValue::Url(u.clone())) {
                                    Some(property_name.to_owned())
                                } else {
                                    None
                                }
                            } else {
                                None
                            }
                        }
                    }
                })
            })
            .collect();

        let children_properties = self
            .children
            .iter()
            .map(|item| {
                item.borrow()
                    .properties_with_matching_value(property_value.clone())
                    .into_iter()
            })
            .flatten()
            .collect::<Vec<_>>();

        let mut items = [self_properties, children_properties]
            .iter()
            .flatten()
            .cloned()
            .collect::<Vec<_>>();

        items.sort();
        items.dedup();
        items
    }

    /// Resolve full date time values for those missing them.
    /// This is an implementation of <https://microformats.org/wiki/value-class-pattern#microformats2_parsers_implied_date>.
    pub fn concatenate_times(&mut self) {
        trace!("Attempting to resolve the implied dates for time values.");

        let mut last_seen_date: Option<temporal::Date> = None;

        self.properties
            .borrow_mut()
            .values_mut()
            .map(|properties| properties.iter_mut())
            .flatten()
            .for_each(|property| {
                if let PropertyValue::Temporal(temporal::Value::Timestamp(timestamp)) = property {
                    if timestamp.date.is_some() {
                        trace!("Recording {:?} as the last seen date.", timestamp.date);
                        last_seen_date = timestamp.date.clone();
                    } else {
                        trace!(
                            "Using {:?} as the date with {:?}",
                            last_seen_date,
                            timestamp.date
                        );
                        timestamp.date = last_seen_date.clone();
                        last_seen_date = None;
                    }
                }
            })
    }
}

#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Default, serde::Serialize, serde::Deserialize,
)]
pub struct Relation {
    pub rels: Vec<String>,

    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub hreflang: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub media: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub text: Option<String>,
}

impl Relation {
    /// Fuses the values of the other relation with this one.
    pub fn merge_with(&mut self, other: Self) {
        self.rels.extend_from_slice(&other.rels);
        self.rels.sort();
        self.rels.dedup();

        if self.hreflang == None {
            self.hreflang = other.hreflang;
        }

        if self.media == None {
            self.media = other.media;
        }
        if self.title == None {
            self.title = other.title;
        }
        if self.r#type == None {
            self.r#type = other.r#type;
        }
        if self.text == None {
            self.text = other.text;
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Default, serde::Deserialize, serde::Serialize)]
pub struct Relations {
    #[serde(flatten)]
    pub items: HashMap<Url, Relation>,
}

impl Relations {
    pub fn by_rels(&self) -> HashMap<String, Vec<Url>> {
        let mut rels: HashMap<String, Vec<Url>> = HashMap::default();
        self.items
            .iter()
            .map(|(u, rel)| {
                rel.rels
                    .iter()
                    .map(move |rel_name| (rel_name.to_owned(), u.to_owned()))
            })
            .flatten()
            .for_each(|(rel_name, url)| {
                if let Some(rel_urls) = rels.get_mut(&rel_name) {
                    rel_urls.push(url);
                } else {
                    rels.insert(rel_name, vec![url]);
                }
            });

        rels.iter_mut().for_each(|(_, urls)| {
            urls.dedup();
            urls.sort()
        });

        rels
    }
}

/// Represents a parsed document of Microformats items and its relating rel links.
// The long-term goal of this is to have the items be a hashmap of all of the
// resolved items by a particular hashable ID. Then for each Item, we have a
// ItemID that can be used to look up the values to reduce the number of unbounded
// pointers made to a central value. It also make the resolving of a particular
// item in a whole page faster and even possible to do anchor-specific resolving
// of items in a document _without_ a provided URL.
// FIXME: Implement serde::Deserialize to build map of items by internal IDs against tree of items.
// FIXME: Re-build the tree for serialization.
#[derive(Clone, Debug, PartialEq, Default, Eq)]
pub struct Document {
    pub items: Vec<Rc<RefCell<Item>>>,
    pub url: Option<url::Url>,
    pub rels: Relations,
    pub lang: Option<String>,
}

impl Document {
    pub fn new(url: Option<Url>) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            url,
            ..Default::default()
        }))
    }

    /// Creates a new top-level child item for this document.
    pub fn create_child_item(&mut self, types: &[Class]) -> Rc<RefCell<Item>> {
        let item = Rc::new(Item::new(Rc::new(ParentRelationship::Root), types.to_vec()));
        self.items.push(Rc::clone(&item));

        Rc::clone(&item)
    }

    pub fn add_relation(&mut self, url: Url, relation: Relation) {
        if let Some(rel) = self.rels.items.get_mut(&url) {
            rel.merge_with(relation);
        } else {
            self.rels.items.insert(url.to_owned(), relation);
        }
    }

    /// Finds the property in this document that's preferred by this URL.
    pub fn get_item_by_url(&self, url: &Url) -> Option<PropertyValue> {
        self.items
            .iter()
            .find_map(|item| Item::has_url(Rc::clone(item), url))
    }

    /// Finds the property in this document that has the provided ID.
    pub fn get_item_by_id(&self, id: &str) -> Option<Rc<RefCell<Item>>> {
        self.items
            .iter()
            .find_map(|item| Item::has_id(Rc::clone(item), id))
    }

    /// Finds the item and property name that this exists in.
    pub fn find_property_name_with_value(
        &self,
        property_value: PropertyValue,
    ) -> Vec<(Rc<RefCell<Item>>, String)> {
        let mut items = self
            .items
            .iter()
            .map(|item| {
                item.borrow()
                    .properties_with_matching_value(property_value.clone())
                    .into_iter()
                    .map(move |name| (Rc::clone(item), name))
            })
            .flatten()
            .collect::<Vec<_>>();

        items.sort();
        items.dedup();
        items
    }
}

impl serde::Serialize for Document {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Document", 3)?;
        let items = self
            .items
            .iter()
            .map(|item| item.borrow().deref().clone())
            .collect::<Vec<_>>();

        s.serialize_field("items", &items)?;
        s.serialize_field("rel-urls", &self.rels.items)?;
        s.serialize_field("rels", &self.rels.by_rels())?;
        if let Some(lang) = &self.lang {
            s.serialize_field("lang", lang)?;
        }
        s.end()
    }
}

#[derive(serde::Deserialize, Debug)]
#[serde(field_identifier, rename_all = "kebab-case")]
enum DocumentDeserializationFields {
    Items,
    RelUrls,
    Rels,
    Url,
    Lang,
}

impl<'de> serde::Deserialize<'de> for Document {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct DocumentVisitor;

        impl<'de> Visitor<'de> for DocumentVisitor {
            type Value = Document;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a Microformat document represented with the expected fields")
            }

            fn visit_map<V>(self, mut map: V) -> Result<Document, V::Error>
            where
                V: de::MapAccess<'de>,
            {
                let mut document = Document::default();
                let mut rel_urls: Option<Relations> = None;

                while let Ok(Some(key)) = map.next_key() {
                    match key {
                        DocumentDeserializationFields::Items => {
                            let raw_items = map.next_value::<Vec<Item>>()?;
                            let items: Vec<Rc<RefCell<Item>>> = raw_items
                                .into_iter()
                                .map(|mut item| {
                                    item.parent = Rc::new(ParentRelationship::Root);
                                    Rc::new(RefCell::new(item))
                                })
                                .collect::<Vec<_>>();

                            document.items.extend(items);
                        }
                        DocumentDeserializationFields::Url => {
                            if document.url.is_some() {
                                return Err(de::Error::duplicate_field("url"));
                            }

                            document.url = map.next_value()?;
                        }
                        DocumentDeserializationFields::RelUrls => {
                            if rel_urls.is_some() {
                                return Err(de::Error::duplicate_field("rel-urls"));
                            }

                            rel_urls = map.next_value()?;
                        }
                        DocumentDeserializationFields::Lang => {
                            if document.lang.is_some() {
                                return Err(de::Error::duplicate_field("lang"));
                            }

                            document.lang = map.next_value()?;
                        }
                        DocumentDeserializationFields::Rels => {
                            map.next_value::<HashMap<String, Vec<String>>>()?;
                        }
                    }
                }

                document.rels = rel_urls.unwrap_or_default();

                Ok(document)
            }
        }

        deserializer.deserialize_struct(
            "Document",
            &["items", "rel-urls", "url", "lang"],
            DocumentVisitor,
        )
    }
}

impl IntoIterator for Document {
    type Item = Item;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.items
            .into_iter()
            .flat_map(|item| item.borrow().clone().into_iter())
            .collect::<Vec<_>>()
            .into_iter()
    }
}

impl From<Document> for ParentRelationship {
    fn from(_: Document) -> Self {
        ParentRelationship::Root
    }
}
